using System.Collections.Generic;
using Meeseeks.Server.Blockly;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Meeseeks.Server.Tests.Blockly
{
    [TestClass]
    public class BlocklyBlockTests
    {
        [TestMethod]
        public void TestJsonSerialisation()
        {
            BlocklyBlock bb = new BlocklyBlock
            {
                Name = "knx_switch",
                Tooltip = "foobar",
                HelpUrl = "google.de",
                ColorHue = 100,
                TextTemplate = "Foo %1 Bar %2",
                Arguments = new List<BlocklyArgument>
                {
                    new BlocklyArgument
                    {
                        Type = BlocklyInputType.Field,
                        ValueName = "foovalue1"
                    },
                    new BlocklyArgument
                    {
                        Type = BlocklyInputType.Dropdown,
                        ValueName = "bardrop",
                        Options = new List<string[]>
                        {
                            new [] {"opt1","OPTION1"},
                            new [] {"opt2","OPTION2"}
                        }
                    }
                }
            };

            //run it though newtonsoft
            string json = JsonConvert.SerializeObject(bb);
            string expected = "{\"type\":\"knx_switch\",\"tooltip\":\"foobar\",\"helpurl\":\"google.de\",\"colour\":100,\"message0\":\"Foo %1 Bar %2\",\"args0\":[{\"type\":\"field_input\",\"name\":\"foovalue1\"},{\"type\":\"field_dropdown\",\"name\":\"bardrop\",\"options\":[[\"opt1\",\"OPTION1\"],[\"opt2\",\"OPTION2\"]]}]}";

            Assert.AreEqual(expected,json);

        }
    }
}
