﻿using System;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Intrgrations;
using Meeseeks.Server.Speech;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Meeseeks.Server.Controllers
{
    [Route("api/door")]
    public class DoorController : Controller
    {
        private readonly string _rabbitHost;

        public DoorController(IConfigurationRoot config)
        {
            _rabbitHost = config["rabbithost"];
        }
        [HttpGet("dingdong")]
        public void RingDoor()
        {
            SpeechSynthesis speech = new SpeechSynthesis(_rabbitHost);
            speech.PlaySoundOnHouse("voy_door_chime_2_dual.mp3", PlaybackLocations.Bedroom | PlaybackLocations.Hallway | PlaybackLocations.LivingRoom);

            string slacktext = $"Um {DateTime.Now:HH:mm} Uhr hat jemand hat an der Haustür geklingelt.";
            SlackIntegration.SendText(slacktext);
        }

        [HttpGet("test")]
        public void TestDoor()
        {
            SpeechSynthesis speech = new SpeechSynthesis(_rabbitHost);
            speech.PlaySoundOnHouse("processing2.mp3", PlaybackLocations.Bedroom | PlaybackLocations.Hallway | PlaybackLocations.LivingRoom);

            string slacktext = "Das ist ein sound test";
            SlackIntegration.SendText(slacktext);
        }
    }
}