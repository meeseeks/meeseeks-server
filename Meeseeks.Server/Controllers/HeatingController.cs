﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Controllers
{
    [Route("api/heating")]
    public class HeatingController : Controller
    {
        private readonly ISensorManager _sensorManager;
        private readonly IKnxSender _ks;
        private readonly IKnxManager _km;
        private readonly ILogger<HeatingController> _logger;

        public HeatingController(ISensorManager sensMgr, IKnxSender knx, IKnxManager knxmgr, ILoggerFactory log)
        {
            _sensorManager = sensMgr;
            _ks = knx;
            _km = knxmgr;
            _logger = log.CreateLogger<HeatingController>();
        }

        // GET api/values
        [HttpGet("verteiler/{etage}")]
        public async Task<Dictionary<string, double>> GetVerteilerAsync(string etage)
        {
            string vorlaufSensor = string.Empty;
            string ruecklaufSensor = string.Empty;
            if (etage == "ug")
            {
                vorlaufSensor = "28.BF7A48050000";
                ruecklaufSensor = "28.A72D75050000";
            }
            else if (etage == "og")
            {
                vorlaufSensor = "28.BF6CD1040000";
                ruecklaufSensor = "28.B09FE0040000";
            }

            double vorlauf = (await _sensorManager.GetCurrentValueAsync(BusType.OneWire, vorlaufSensor)).Value;
            double rueck = (await _sensorManager.GetCurrentValueAsync(BusType.OneWire, ruecklaufSensor)).Value;

            return new Dictionary<string, double>
            {
                {"vorlauf", vorlauf},
                {"rueck", rueck}
            };
        }

        [HttpGet("roomstatus")]
        public async Task<List<Dictionary<string, object>>> GetRoomAsync()
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>
            {
                await GetRoomStatusAsync("finn"),
                await GetRoomStatusAsync("eat"),
                await GetRoomStatusAsync("jasmin"),
                await GetRoomStatusAsync("kitchen"),
                await GetRoomStatusAsync("living"),
                await GetRoomStatusAsync("bath"),
                await GetRoomStatusAsync("bath2"),
                await GetRoomStatusAsync("office"),
                await GetRoomStatusAsync("sleep"),
                await GetRoomStatusAsync("ankleide"),
                await GetRoomStatusAsync("flur")
            };
            return result;
        }

        private async Task<Dictionary<string, object>> GetRoomStatusAsync(string room)
        {
            double ventil = 0;
            double sollSensor = 0;
            double istSensor = 0;

            switch (room)
            {
                case "finn":
                    ventil = await GetValueAsync(BusType.Knx, "13/4/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "13/4/1"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "13/4/3"); //DPT 9.001
                    break;
                case "eat":
                    ventil = await GetValueAsync(BusType.Knx, "10/5/3", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "10/5/0"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "10/5/1"); //DPT 9.001
                    break;
                case "jasmin":
                    ventil = await GetValueAsync(BusType.Knx, "12/4/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "12/4/1"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "12/4/3"); //DPT 9.001
                    break;
                case "kitchen":
                    ventil = await GetValueAsync(BusType.Knx, "11/4/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "11/4/1"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "11/4/3"); //DPT 9.001
                    break;
                case "living":
                    ventil = await GetValueAsync(BusType.Knx, "9/3/1", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "10/5/0"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "10/5/1"); //DPT 9.001
                    break;
                case "bath":
                    ventil = await GetValueAsync(BusType.Knx, "15/2/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "15/2/0"); //DPT 9.001
                    istSensor = (await _sensorManager.GetCurrentValueAsync(BusType.OneWire, "28.545DD2040000")).Value; //DPT 9.001
                    break;
                case "bath2":
                    ventil = await GetValueAsync(BusType.Knx, "8/2/0", KnxDataPoint.Dpt5); //DPT 5.001
                    //sollSensor = await GetValue(BusType.Knx, "13/4/1"); //DPT 9.001
                    //istSensor = await GetValue(BusType.Knx, "13/4/3"); //DPT 9.001
                    break;
                case "office":
                    ventil = await GetValueAsync(BusType.Knx, "6/3/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "6/3/1"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "6/3/3"); //DPT 9.001
                    break;
                case "sleep":
                    ventil = await GetValueAsync(BusType.Knx, "3/5/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "3/5/1"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "3/5/3"); //DPT 9.001
                    break;

                case "ankleide":
                    ventil = await GetValueAsync(BusType.Knx, "7/3/3", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "7/3/2"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "7/3/0"); //DPT 9.001
                    break;

                case "flur":
                    ventil = await GetValueAsync(BusType.Knx, "13/4/2", KnxDataPoint.Dpt5); //DPT 5.001
                    sollSensor = await GetValueAsync(BusType.Knx, "13/4/1"); //DPT 9.001
                    istSensor = await GetValueAsync(BusType.Knx, "13/4/3"); //DPT 9.001
                    break;
            }
            return new Dictionary<string, object>
            {
                {"ventil", Math.Round(ventil,1)},
                {"sollSensor", Math.Round(sollSensor,1)},
                {"istSensor", Math.Round(istSensor,1)},
                {"id", room}
            };

        }

        private async Task<double> GetValueAsync(BusType bus, string device, KnxDataPoint dpt = KnxDataPoint.Notset)
        {
            try
            {
                var sv = await _sensorManager.GetCurrentValueAsync(bus, device);

                if (bus != BusType.Knx)
                {
                    if (sv != null) return sv.Value;
                    _logger.LogDebug($"Got no Sensor Value for {device}");
                    return -2;
                }

                //KNX
                var ga = await _km.FindGroupAddress(device);

                double value;

                if (sv == null || sv.RecordTime < DateTime.UtcNow.AddMinutes(-2))
                {
                    //Read from bus
                    string strVal = _ks.ReadGroup(device, ga.DptFormat);
                    _logger.LogDebug($"Pulled KNX Sensor {ga} value from bus:{strVal}");

                    value = double.Parse(strVal);
                }
                else
                {
                    value = sv.Value;
                }

                if (dpt == KnxDataPoint.Dpt5)
                {
                    value = (value / 255.0) * 100;
                }
                else if (ga.DptFormat == KnxDataPoint.Dpt5)
                {
                    value = (value / 255.0) * 100;
                }

                return value;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return -1;
            }
        }
    }
}
