using System;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Logics;
using Meeseeks.Server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Controllers
{
    public class WebEventController : Controller
    {
        private readonly ILogicEngine _logic;
        private readonly ILogger<WebEventController> _logger;
        private IServiceProvider _services;

        public WebEventController(ILoggerFactory log, ILogicEngine logic, IServiceProvider serv)
        {
            _logic = logic;
            _services = serv;
            _logger = log.CreateLogger<WebEventController>();

        }

        [HttpGet("api/trigger/{name}")]
        public void Trigger(string name)
        {
            _logger.LogDebug($"Trigger logic from web: {name}");
            WebEventArgs args = new WebEventArgs(name);
            _logic.ProcessEvent(args);
        }


        [HttpGet("api/test/wakeup")]
        public void WakeUp()
        {
            WakeUpMode wum = new WakeUpMode(_services);
            wum.WakeHouseAndSoundAlarmClock(DateTime.Now);
        }

    }
}