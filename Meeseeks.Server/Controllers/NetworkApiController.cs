﻿using System;
using Microsoft.AspNetCore.Mvc;
using Renci.SshNet;

namespace Meeseeks.Server.Controllers
{
    [Route("api/network")]
    public class NetworkApiController : Controller
    {
        [HttpGet("shutdown-ssh")]
        public ActionResult ShutdownViaSsh([FromQuery] string host, [FromQuery] string user, [FromQuery] string password)
        {
            //Shutdown Laforge
            try
            {
                //Shutdown Laforge
                using (var client = new SshClient(host, user, password))
                {
                    client.Connect();
                    client.RunCommand("poweroff");
                    client.Disconnect();
                }
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    msg = "Unable to shut down host",
                    error = e.Message,
                    host,user
                });
            }
            return Ok(new
            {
                msg = "Shutdown triggered",
                host,
                user
            });


        }
    }

}
