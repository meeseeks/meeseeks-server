﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Blockly;
using Meeseeks.Server.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Meeseeks.Server.Controllers
{
    [Route("api/blockly")]
    public class BlocklyController : Controller
    {
        private readonly IBlocklyRepository _blockRepo;

        public BlocklyController(IBlocklyRepository blockRepo)
        {
            _blockRepo = blockRepo;
        }

        [HttpGet("blocks")]
        public async Task<string> GetAllBlocks()
        {
            List<BlocklyBlock> blocks = await _blockRepo.FetchAll();
            return JsonConvert.SerializeObject(blocks);
        }

    }
}