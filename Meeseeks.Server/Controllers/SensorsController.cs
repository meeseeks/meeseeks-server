using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Models;
using Meeseeks.Server.Plot;
using Microsoft.AspNetCore.Mvc;
using SharpGoogleCharts;

namespace Meeseeks.Server.Controllers
{
    [Route("api/sensors")]
    public class SensorsController : Controller
    {
        private readonly ISensorManager _sensorMgr;

        public SensorsController(ISensorManager smgr)
        {
            _sensorMgr = smgr;
        }

        [HttpGet("{sensorid:guid}/latest")]
        public async Task<IActionResult> GetLatestValue(Guid sensorid)
        {
            var sensor = await _sensorMgr.GetSensorAsync(sensorid);
            var value = await _sensorMgr.GetCurrentValueAsync(sensor.Bus,sensor.Address);
            return Ok(value);
        }

        [HttpGet("list")]
        public async Task<List<Sensor>> GetSensorsAsync()
        {
            return await _sensorMgr.GetSensors();
        }

        [HttpPost("plot")]
        public async Task<object> GetPlotAsync([FromBody]PlotRequestModel prm)
        {

            ChartOptions opts = new ChartOptions
            {
                ChartType = PlotChartType.Line,
                Resolution = PlotResolution.Day,
                Showgrid = true,
                XAxis = { Datatype = AxisDataType.DateTime, Label = "Datum" },
                YAxis = { Datatype = AxisDataType.Numeric, Label = "Kosten" },
                Showlegend = false,
                Curves = { new ChartCurveOptions { Color = "#d9b310" } }

            };

            GoogleChart chart = new GoogleChart { ChartDefinition = opts };

            IEnumerable<SensorValue> values = await _sensorMgr.GetSensorValuesAsync(prm.GetBusType(), prm.Sensor, DateTime.Now.AddDays(prm.Days * -1), DateTime.Now);

            var plotValues = new List<PlotPoint>();
            foreach (var v in values.GroupBy(x => x.RecordTime.DayOfYear).Select(cl => new SensorValue { RecordTime = cl.First().RecordTime, Value = cl.Sum(x => x.Value) }))
            {
                if (v.Value < 0) v.Value = 0;
                plotValues.Add(new PlotPoint(v.RecordTime, v.Value) { PrettyY = $"{v.Value:F2} Eur" });
            }
            chart.CurveData.Add("Wasser", plotValues);

            return new { chart = chart.GetGoogleDataTable(), opt = chart.GetGoogleOptions(), total = $"{plotValues.Sum(x => x.Y):F2} Eur" };
        }

        [HttpPost("save")]
        public void SaveSensor([FromBody] Sensor sensor)
        {
            _sensorMgr.SaveSensor(sensor);
        }

        [HttpGet("{sensorid:guid}")]
        public async Task<Sensor> SaveSensorAsync(Guid sensorid)
        {
            return await _sensorMgr.GetSensorAsync(sensorid);
        }

        [HttpPost("plotbyid")]
        public async Task<object> GetPlotByIdAsync([FromBody]PlotByIdRequestModel prm)
        {
            switch (prm.Options.Mode)
            {
                case "time":
                    return await TimePlotByDayAsync(prm);
                case "range":
                    return await RangePlotByHourAsync(prm);
            }

            throw new Exception("unkown plot mode");
        }

        private async Task<object> RangePlotByHourAsync(PlotByIdRequestModel prm)
        {
            ChartOptions opts = new ChartOptions
            {
                ChartType = PlotChartType.Line,
                Resolution = PlotResolution.Hour,
                Showgrid = true,
                XAxis = { Datatype = AxisDataType.Numeric, Label = "Stunde" },
                YAxis = { Datatype = AxisDataType.Numeric, Label = "Kosten" },
                Showlegend = false,
                Curves = { new ChartCurveOptions { Color = "#d9b310" } }
            };
            GoogleChart chart = new GoogleChart { ChartDefinition = opts };

            foreach (var sid in prm.SensorId)
            {
                Sensor s = await _sensorMgr.GetSensorAsync(sid);
                SensorTypeOptions st = SensorTypeOptions.GetOptions(s.Measure);
                IEnumerable<SensorValue> values = await _sensorMgr.GetSensorValuesAsync(sid,
                    DateTime.Now.AddYears(-1),
                    DateTime.Now);

                var avgValues = new List<PlotPoint>();
                var minValues = new List<PlotPoint>();
                var maxValues = new List<PlotPoint>();
                foreach (var v in values
                    .GroupBy(x => new { x.RecordTime.Hour })
                    .Select(cl => new RangePlot
                    {
                        Hour = cl.First().RecordTime.Hour,
                        AvgValue = cl.Average(x => x.Value),
                        MinValue = cl.Min(x => x.Value),
                        MaxValue = cl.Max(x => x.Value)
                    }).OrderBy(x => x.Hour))
                {
                    avgValues.Add(new PlotPoint(v.Hour, v.AvgValue) { PrettyY = $"{v.AvgValue:F2}{st.Unit}" });
                    minValues.Add(new PlotPoint(v.Hour, v.MinValue) { PrettyY = $"{v.MinValue:F2}{st.Unit}" });
                    maxValues.Add(new PlotPoint(v.Hour, v.MaxValue) { PrettyY = $"{v.MaxValue:F2}{st.Unit}" });
                }
                chart.CurveData.Add(s.Description ?? $"Unkown #{chart.CurveData.Count}" + " Avg", avgValues);
                chart.CurveData.Add(s.Description ?? $"Unkown #{chart.CurveData.Count}" + " Min", minValues);
                chart.CurveData.Add(s.Description ?? $"Unkown #{chart.CurveData.Count}" + " Max", maxValues);
            }

            return
                new
                {
                    chart = chart.GetGoogleDataTable(),
                    opt = chart.GetGoogleOptions()
                };
        }

        private async Task<object> TimePlotByDayAsync(PlotByIdRequestModel prm)
        {

            PlotGenerator gen = new PlotGenerator();
            var opts = gen.ConvertPlotOptions(prm.Options);

            GoogleChart chart = new GoogleChart { ChartDefinition = opts };

            foreach (var sid in prm.SensorId)
            {
                chart.ChartDefinition.Curves.Add(new ChartCurveOptions { Color = "#d9b310" });
                //pull sensor data
                List<PlotPoint> points = await _sensorMgr.AggregateForPlotAsync(sid, prm.Options);
                Sensor s = await _sensorMgr.GetSensorAsync(sid);
                opts.Curves.Add(new ChartCurveOptions { Color = "#d9b310" });
                chart.CurveData.Add(s.Description ?? $"Unkown #{chart.CurveData.Count}", points);
            }
            return
                new
                {
                    chart = chart.GetGoogleDataTable(),
                    opt = chart.GetGoogleOptions()
                };
        }
    }
}
