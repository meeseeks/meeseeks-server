using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Controllers
{
    [Route("api/knx")]
    public class KnxController : Controller
    {
        private readonly ILogger<KnxController> _logger;
        //private readonly IConnectionManager _signalr;
        private readonly IKnxManager _knxMgr;
        private readonly ISensorManager _sensorMgr;
        private readonly ILogicEngine _logic;

        public KnxController(ILoggerFactory log, IKnxManager knxmgr, ISensorManager sensorMgr, ILogicEngine logic)
        {
            _logger = log.CreateLogger<KnxController>();
            //_signalr = signalrConnectionManager;
            _knxMgr = knxmgr;
            _sensorMgr = sensorMgr;
            _logic = logic;
        }

        [HttpGet("groupaddress/list")]
        public async Task<IEnumerable<KnxGroupAddress>> GetGroupAddressesAsync()
        {
            return await _knxMgr.ListAllGroupAdresses();
        }

        [HttpPost("event")]
        public void RecieveKnxEvent([FromBody] KnxEvent busmsg)
        {
            if (busmsg == null) return;
            //_logger.LogDebug($"Recieved Knx Event: {busmsg.Destination} => {busmsg.Value}");

            //Send to signalr
            //var context = _signalr.GetHubContext<SensorHub>();
            //context.Clients.All.KnxEvent(new { address = busmsg.Destination, value = busmsg.Value });

            KnxGroupAddress ga = _knxMgr.FindGroupAddress(busmsg.Destination).Result;

            KnxDataPoint dpt = (KnxDataPoint)Enum.Parse(typeof(KnxDataPoint), busmsg.DataType, true);

            if (dpt != ga.DptFormat)
            {
                _logger.LogWarning($"Updated GA type for {busmsg.Destination} => {busmsg.DataType}");

                ga.DptFormat = dpt;
                _knxMgr.UpdateGa(ga);
            }


            if (double.TryParse(busmsg.Value.ToString(), out double sensorValue))
            {
                _sensorMgr.RecordValueAsync(BusType.Knx, busmsg.Destination, sensorValue);
            }

            //kick of any possible logics
            KnxBusEventArgs knxargs = new KnxBusEventArgs(ga, busmsg.Value.ToString());
            _logic.ProcessEvent(knxargs);

        }
    }
}