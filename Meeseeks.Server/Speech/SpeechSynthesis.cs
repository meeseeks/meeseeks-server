using System.Text;
using Meeseeks.Server.Models;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Renci.SshNet;

namespace Meeseeks.Server.Speech
{

    public class SpeechSynthesis
    {
        private readonly ConnectionFactory _rabbitFactory;
        private const string SpeechHost = "172.16.20.177";
        private const string SpeechHostUser = "markus";
        private const string SpeechHostPass = "9gJodFii";

        public SpeechSynthesis(string rabbitHost)
        {
            _rabbitFactory = new ConnectionFactory { HostName = rabbitHost };

        }

        public void PlayRadio()
        {
            using (var client = new SshClient(SpeechHost, SpeechHostUser, SpeechHostPass))
            {
                client.Connect();
                client.RunCommand("screen -d -m ~/playradio.sh");
                client.Disconnect();
            }
        }

        public void ControlRadio(string cmd)
        {
            string sshcmd = $"echo \"{cmd}\" > ~/mpcontrol";
            if (cmd == "quit")
            {
                sshcmd = "killall mplayer";
            }
            using (var client = new SshClient(SpeechHost, SpeechHostUser, SpeechHostPass))
            {
                client.Connect();
                client.RunCommand(sshcmd);
                client.Disconnect();
            }
        }

        public void PlaySoundOnHouse(string sound, PlaybackLocations location)
        {
            using (var connection = _rabbitFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare("playsound", true, false, false, null);

                if ((PlaybackLocations.Bedroom & location) == PlaybackLocations.Bedroom)
                {
                    SendSoundToRabbit(sound, channel, 0);
                }
                if ((PlaybackLocations.Hallway & location) == PlaybackLocations.Hallway)
                {
                    SendSoundToRabbit(sound, channel, 1);
                }
                if ((PlaybackLocations.Office & location) == PlaybackLocations.Office)
                {

                }
                if ((PlaybackLocations.LivingRoom & location) == PlaybackLocations.LivingRoom)
                {
                    SendSoundToRabbit(sound, channel, 2);
                }
            }
        }

        private static void SendSoundToRabbit(string sound, IModel channel, int device)
        {
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new RedisSoundEvent
            {
                Device = device,
                Soundfile = sound
            }));

            channel.BasicPublish(exchange: "",
                routingKey: "playsound",
                basicProperties: null,
                body: body);
        }

        private static void SendSpeechToRabbit(string text, IModel channel, int device)
        {
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new RedisSpeechEvent
            {
                Device = device,
                TextToSpeak = text
            }));

            channel.BasicPublish(exchange: "",
                routingKey: "playspeech",
                basicProperties: null,
                body: body);
        }

        public void SpeakToHouse(string text, PlaybackLocations location)
        {
            using (var connection = _rabbitFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare("playspeech", true, false, false, null);


                if ((PlaybackLocations.Bedroom & location) == PlaybackLocations.Bedroom)
                {
                   SendSpeechToRabbit(text,channel,0);
                }
                if ((PlaybackLocations.Hallway & location) == PlaybackLocations.Hallway)
                {
                    SendSpeechToRabbit(text, channel, 1);
                }
                if ((PlaybackLocations.Office & location) == PlaybackLocations.Office)
                {

                }

                if ((PlaybackLocations.LivingRoom & location) == PlaybackLocations.LivingRoom)
                {
                    SendSpeechToRabbit(text, channel, 2);
                }
            }
        }
    }
}