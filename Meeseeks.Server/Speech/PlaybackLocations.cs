﻿using System;

namespace Meeseeks.Server.Speech
{
    [Flags]
    public enum PlaybackLocations
    {
        Bedroom = 1,
        Hallway = 2,
        Office = 4,
        LivingRoom = 8
    }
}