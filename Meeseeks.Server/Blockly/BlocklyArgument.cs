﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Meeseeks.Server.Blockly
{
    public class BlocklyArgument
    {
        [JsonProperty("type")]
        public BlocklyInputType Type { get; set; }

        [JsonProperty("name")]
        public string ValueName { get; set; }
        
        [JsonProperty("options",NullValueHandling = NullValueHandling.Ignore)]
        public List<string[]> Options { get; set; }
    }
}