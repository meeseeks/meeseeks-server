﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Meeseeks.Server.Blockly
{
    public class BlocklyInputTypeConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is BlocklyInputType))
            {
                throw new ArgumentException("This Converter only works on blocklyInputType enums");
            }

            string typeStr = string.Empty;
            BlocklyInputType bit = (BlocklyInputType)value;
            switch (bit)
            {
                case BlocklyInputType.Field:
                    typeStr = "field_input";
                    break;
                case BlocklyInputType.Dummy:
                    typeStr = "input_dummy";
                    break;
                case BlocklyInputType.Dropdown:
                    typeStr = "field_dropdown";
                    break;
            }


            JValue o = new JValue(typeStr);
            o.WriteTo(writer);

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.String)
            {
                throw new JsonSerializationException();
            }

            string value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "field_input":
                    return BlocklyInputType.Field;
                case "input_dummy":
                    return BlocklyInputType.Dummy;
                case "field_dropdown":
                    return BlocklyInputType.Dropdown;
            }
            throw new ArgumentOutOfRangeException($"Unable to deserialize {value} as a BlocklyInputType");
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(BlocklyInputType);
        }
    }
}