﻿using Newtonsoft.Json;

namespace Meeseeks.Server.Blockly
{
    [JsonConverter(typeof(BlocklyInputTypeConverter))]
    public enum BlocklyInputType
    {
        Field = 0,
        Dummy = 1,
        Dropdown = 2
    }
}