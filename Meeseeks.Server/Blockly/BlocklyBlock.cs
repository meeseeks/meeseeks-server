﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Meeseeks.Server.Blockly
{
    public class BlocklyBlock
    {
        [JsonProperty("type")]
        public string Name { get; set; }

        [JsonProperty("tooltip")]
        public string Tooltip { get; set; }

        [JsonProperty("helpurl")]
        public string HelpUrl { get; set; }

        [JsonProperty("colour")]
        public int ColorHue { get; set; }

        [JsonProperty("message0")]
        public string TextTemplate { get; set; }

        [JsonProperty("args0")]
        public List<BlocklyArgument> Arguments { get; set; }
    }
}
