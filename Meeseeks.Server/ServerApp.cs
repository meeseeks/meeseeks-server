﻿using System;
using System.Threading.Tasks;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Models;
using Meeseeks.Server.Watchers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server
{
    public class ServerApp
    {

        private readonly ILogicEngine _logic;
        private readonly ILogger<ServerApp> _logger;
        private readonly IServiceProvider _services;
        private readonly IWebSocketHandler _websockets;

        public ServerApp(IServiceProvider services)
        {
            _services = services;
            ILoggerFactory loggerFactory = _services.GetRequiredService<ILoggerFactory>();
            _logger = loggerFactory.CreateLogger<ServerApp>();
            _logic = _services.GetRequiredService<ILogicEngine>();
            _websockets = services.GetService<IWebSocketHandler>();
            _logger.LogInformation("Application created successfully.");
           
        }


        public void RunServer()
        {
            _logger.LogInformation("Starting Meeseeks Server");

            _logger.LogInformation("Starting OneWire Watch");
            OneWireWatch oww = new OneWireWatch(_services);
            oww.SensorRead += (sender, eventArgs) =>
            {
                //Send over websocket
                _websockets?.SendMessage(new WebSocketMessage
                {
                    Name = "onewire",
                    Payload = new
                    {
                        address = eventArgs.Sensor.Address,
                        value = eventArgs.CurrentValue
                    }
                });

                _logic.ProcessEvent(eventArgs);
            };
            Task.Factory.StartNew(() => { oww.WatchAsync(); });


            //Timer watch
            _logger.LogInformation("Starting Timer Watch");
            TimeWatch tw = new TimeWatch(_services);
            tw.TimerElapsed += (sender, eventArgs) =>
            {
                _logic.ProcessEvent(eventArgs);
            };
            Task.Factory.StartNew(() => { tw.Watch(); });

            ////Redis watch
            //_logger.LogInformation("Starting Redis Watch");
            //RedisWatch redisWatch = new RedisWatch(_services);
            //redisWatch.OnKnxEvent += (sender, eventArgs) =>
            //{
            //    _logic.ProcessEvent(eventArgs);
            //};
            //Task.Factory.StartNew(() => { redisWatch.Watch(); });

            //Redis watch
            _logger.LogInformation("Starting KNX RabbitMQ Watch");
            KnxRabbitWatch rabbitWatch = new KnxRabbitWatch(_services);
            rabbitWatch.OnKnxEvent += (sender, eventArgs) =>
            {
                
                _logic.ProcessEvent(eventArgs);
            };
            Task.Factory.StartNew(() =>
            {
                
                rabbitWatch.Watch();

            });

            

        }

    }
}