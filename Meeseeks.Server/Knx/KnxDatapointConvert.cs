using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Meeseeks.Server.Knx
{
    public static class KnxDatapointConvert
    {
        private static bool Dpt1(string rawValue)
        {
            return rawValue == "01";
        }

        private static char Dpt4(string rawValue)
        {
            return (char)Convert.ToInt32("0x" + rawValue, 16);
        }

        private static byte Dpt5(string rawValue)
        {
            return byte.Parse(rawValue, NumberStyles.HexNumber);
        }

        private static sbyte Dpt6(string rawValue)
        {
            return sbyte.Parse(rawValue, NumberStyles.HexNumber);
        }

        private static short Dpt7(string rawValue)
        {
            return short.Parse(rawValue.Replace(" ", ""), NumberStyles.HexNumber);
        }

        /// <summary>
        /// Converts KNX Data with DPT9 (2byte float) to double
        /// </summary>
        /// <param name="value">KNX data</param>
        /// <returns>DPT9 value as double</returns>
        private static double Dpt9(string value)
        {
            string rawvalue = Regex.Replace(value, @"\s", "");
            int bin1 = Convert.ToInt16(rawvalue.Substring(0, 2), 16);   //first byte
            int bin2 = Convert.ToInt16(rawvalue.Substring(2, 2), 16);   //second byte
            int sign = Convert.ToInt16(bin1 & 0x80);
            int exp = Convert.ToInt16(bin1 & 0x78) >> 3;

            int mant = Convert.ToInt16(((bin1 & 0x7) << 8) | bin2);
            if (sign != 0)
                mant = -(~(mant - 1) & 0x7ff);
            double dez = (1 << exp) * 0.01 * mant;
            return dez;
        }

        //time
        private static KnxTime Dpt10(string rawValue)
        {
            KnxTime dt = new KnxTime();
            rawValue = rawValue.Replace(" ", "");

            byte firstbyte = byte.Parse(rawValue.Substring(0, 1), NumberStyles.HexNumber);

            string hourbytes = rawValue.Substring(0, 2);

            if (firstbyte > 1) //weekday encoded
            {
                if (firstbyte == 2 || firstbyte == 3)
                    dt.Day = DayOfWeek.Monday;
                else if (firstbyte == 4 || firstbyte == 5)
                    dt.Day = DayOfWeek.Tuesday;
                else if (firstbyte == 6 || firstbyte == 7)
                    dt.Day = DayOfWeek.Wednesday;
                else if (firstbyte == 8 || firstbyte == 9)
                    dt.Day = DayOfWeek.Thursday;
                else if (firstbyte == 10 || firstbyte == 11)
                    dt.Day = DayOfWeek.Friday;
                else if (firstbyte == 12 || firstbyte == 13)
                    dt.Day = DayOfWeek.Saturday;
                else if (firstbyte == 14 || firstbyte == 15)
                    dt.Day = DayOfWeek.Sunday;

                //remove weekday from hourbytes
                if (firstbyte % 2 == 0)
                    hourbytes = "0" + rawValue.Substring(1, 1);
                else
                    hourbytes = "1" + rawValue.Substring(1, 1);
            }

            dt.Hour = Convert.ToInt16(hourbytes, 16);   //first byte
            dt.Minute = Convert.ToInt16(rawValue.Substring(2, 2), 16);   //second byte
            dt.Second = Convert.ToInt16(rawValue.Substring(4, 2), 16);   //third byte

            return dt;
        }

        private static DateTime Dpt11(string rawValue)
        {
            rawValue = rawValue.Replace(" ", "");
            int day = Convert.ToInt16(rawValue.Substring(0, 2), 16);   //third byte
            int month = Convert.ToInt16(rawValue.Substring(2, 2), 16);   //second byte
            int year = Convert.ToInt16(rawValue.Substring(4, 2), 16) + 2000;   //third byte

            DateTime dt = new DateTime(year, month, day);

            return dt;
        }

        private static float Dpt13(string rawValue)
        {
            return int.Parse(rawValue.Replace(" ", ""), NumberStyles.HexNumber);
        }

        private static float Dpt14(string rawValue)
        {
            uint num = uint.Parse(rawValue.Replace(" ", ""), NumberStyles.AllowHexSpecifier);
            byte[] floatVals = BitConverter.GetBytes(num);
            return BitConverter.ToSingle(floatVals, 0);
        }

        private static string Dpt16(string rawValue)
        {
            string outStr = string.Empty;
            string[] hexValuesSplit = rawValue.Split(' ');
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (string hex in hexValuesSplit)
            {
                // Convert the number expressed in base-16 to an integer.
                int value = Convert.ToInt32(hex, 16);
                // Get the character corresponding to the integral value.
                string stringValue = char.ConvertFromUtf32(value);
                outStr = outStr + stringValue;
            }
            return outStr.TrimEnd(); //cut of any blank bytes
        }

        public static string FromBus(string rawvalue, KnxDataPoint dpt)
        {
            switch (dpt)
            {
                case KnxDataPoint.Notset:
                    return rawvalue;

                case KnxDataPoint.Dpt1:
                    return Dpt1(rawvalue).ToString();

                case KnxDataPoint.Dpt4:
                    return Dpt4(rawvalue).ToString();

                case KnxDataPoint.Dpt5:
                    return Dpt5(rawvalue).ToString();

                case KnxDataPoint.Dpt6:
                    return Dpt6(rawvalue).ToString();

                case KnxDataPoint.Dpt7:
                    return Dpt7(rawvalue).ToString();

                case KnxDataPoint.Dpt9:
                    return Dpt9(rawvalue).ToString(CultureInfo.InvariantCulture);

                case KnxDataPoint.Dpt10:
                    return Dpt10(rawvalue).ToString();

                case KnxDataPoint.Dpt11:
                    return Dpt11(rawvalue).ToString(CultureInfo.InvariantCulture);
                case KnxDataPoint.Dpt13:
                    return Dpt13(rawvalue).ToString(CultureInfo.InvariantCulture);

                case KnxDataPoint.Dpt14:
                    return Dpt14(rawvalue).ToString(CultureInfo.InvariantCulture);

                case KnxDataPoint.Dpt16:
                    return Dpt16(rawvalue);

                default:
                    throw new Exception("Requested DPT not implemented");
            }
        }

        public static string ToBus(object value, KnxDataPoint dpt)
        {
            string outval = "";

            if (dpt == KnxDataPoint.Dpt1)
            {
                if (value is string)
                    value = bool.Parse((string)value);

                if ((bool)value)
                    outval = "0x1";
                else
                    outval = "0x0";
            }
            else if (dpt == KnxDataPoint.Dpt4)
            {
                if (value is string)
                    value = char.Parse((string)value);

                outval = "0x" + Convert.ToInt32((char)value).ToString("X");
            }
            else if (dpt == KnxDataPoint.Dpt5)
            {
                if (value is string)
                    value = byte.Parse((string)value);
                outval = "0x" + ((byte)value).ToString("X");
            }
            else if (dpt == KnxDataPoint.Dpt6)
            {
                if (value is string)
                    value = sbyte.Parse((string)value);
                outval = "0x" + ((sbyte)value).ToString("X");
            }
            else if (dpt == KnxDataPoint.Dpt7)
            {
                if (value is string)
                    value = short.Parse((string)value);
                string hex = ((short)value).ToString("X");
                outval = "0x" + hex.Substring(0, 2) + " 0x" + hex.Substring(2, 2);
            }
            else if (dpt == KnxDataPoint.Dpt9)
            {
                if (value is string)
                    value = double.Parse((string)value);
                double fval = (double)value;
                int sign = 0x0000;
                int exp = 0;

                if (fval < 0)
                    sign = 0x8000;
                var mant = (int)(fval * 100.0);
                while (Math.Abs(mant) > 2047)
                {
                    mant = mant >> 1;
                    exp++;
                }
                int i = sign | (exp << 11) | (mant & 0x07ff);
                int b1 = i >> 8;
                int b2 = i & 0xff;

                //string hex = BitConverter.ToInt16(BitConverter.GetBytes(sval), 0).ToString("X");
                outval = $"{b1:X} {b2:X}";
            }
            else if (dpt == KnxDataPoint.Dpt13)
            {
                if (value is string)
                    value = int.Parse((string)value);
                string hex = ((int)value).ToString("X");
                outval = "0x" + hex.Substring(0, 2) + " 0x" + hex.Substring(2, 2);
            }
            else if (dpt == KnxDataPoint.Dpt14)
            {
                if (value is string)
                    value = float.Parse((string)value);
                float sval = (float)value;
                string hex = BitConverter.ToInt32(BitConverter.GetBytes(sval), 0).ToString("X");
                if (hex.Length == 7)
                    hex = "0" + hex;
                outval = "0x" + hex.Substring(0, 2) + " 0x" + hex.Substring(2, 2) + " 0x" + hex.Substring(4, 2) + " 0x" +
                         hex.Substring(6, 2);
            }

            return outval;
        }

        public static string GetPrettyPrint(KnxDataPoint dpt)
        {
            switch (dpt)
            {
                case KnxDataPoint.Notset:
                    return "Unbekannt";

                case KnxDataPoint.Dpt1:
                    return "Boolean (An/Aus)";

                case KnxDataPoint.Dpt2:
                    return "Boolean (An/Aus) mit Prioritšt";

                case KnxDataPoint.Dpt4:
                    return "Zeichen";

                case KnxDataPoint.Dpt5:
                    return "8-Bit unsigned (DPT5)";

                case KnxDataPoint.Dpt6:
                    return "8-Bit signed (DPT6)";

                case KnxDataPoint.Dpt7:
                    return "16-Bit unsigned (DPT7)";

                case KnxDataPoint.Dpt8:
                    return "16-Bit signed (DPT8)";

                case KnxDataPoint.Dpt9:
                    return "16-Bit float (DPT9)";

                case KnxDataPoint.Dpt10:
                    return "Zeit";

                case KnxDataPoint.Dpt11:
                    return "Datum";

                case KnxDataPoint.Dpt12:
                    return "32-bit signed (DPT12)";

                case KnxDataPoint.Dpt13:
                    return "32-bit unsigned (DPT13)";

                case KnxDataPoint.Dpt14:
                    return "32-bit float (DPT14)";

                case KnxDataPoint.Dpt16:
                    return "Zeichenkette";
            }
            return "Unbekannt";
        }
    }
}