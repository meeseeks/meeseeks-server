using System;

namespace Meeseeks.Server.Knx
{
    public class KnxTime
    {
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }
        public DayOfWeek Day { get; set; }
        public KnxTime()
        {
            Day = DayOfWeek.Monday;
        }
        public override string ToString()
        {
            return $"{Day}, {Hour}:{Minute}:{Second}";
        }
    }
}