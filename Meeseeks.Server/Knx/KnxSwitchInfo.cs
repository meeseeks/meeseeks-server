namespace Meeseeks.Server.Knx
{
    public class KnxSwitchInfo
    {
        public string Switch { get; set; }
        public string Led { get; set; }
    }
}