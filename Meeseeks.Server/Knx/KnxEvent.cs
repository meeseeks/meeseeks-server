using Newtonsoft.Json;

namespace Meeseeks.Server.Knx
{
    public class KnxEvent
    {
        [JsonProperty("src")]
        public string Source { get; set; }

        [JsonProperty("dst")]
        public string Destination { get; set; }

        [JsonProperty("dpt")]
        public string DataType { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }
}