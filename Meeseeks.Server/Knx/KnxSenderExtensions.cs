using Meeseeks.Server.Interfaces;

namespace Meeseeks.Server.Knx
{
    public static class KnxSenderExtensions
    {
        //High Level
        public static void SwitchOff(this IKnxSender ks, string ga)
        {
            ks.WriteGroup(ga, "0", KnxDataPoint.Dpt1);
        }

        public static void SwitchOn(this IKnxSender ks, string ga)
        {
            ks.WriteGroup(ga, "1", KnxDataPoint.Dpt1);
        }
        public static void DimLights(this IKnxSender ks, string ga, double percent)
        {
            int busval = (int)(percent / 100.0 * 255.0);
            ks.WriteGroup(ga, busval.ToString(), KnxDataPoint.Dpt5);
        }

        public static bool ReadSwitch(this IKnxSender ks, string ga)
        {
            return ks.ReadGroup(ga, KnxDataPoint.Dpt1) == "1";
        }
    }
}