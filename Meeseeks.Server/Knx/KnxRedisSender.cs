using Meeseeks.Server.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Meeseeks.Server.Knx
{
    public class KnxRedisSender : IKnxSender
    {
        private readonly bool _sendEnabled;
        private readonly ILogger<KnxHttpSender> _logger;
        private readonly ConnectionMultiplexer _redis;
        private readonly KnxHttpSender _khs;

        public KnxRedisSender(IConfigurationRoot config, ILoggerFactory fac, IRedisConnection rcon)
        {
            _logger = fac.CreateLogger<KnxHttpSender>();
            _sendEnabled = bool.Parse(config["Knx:EnableSend"]);
            _khs = new KnxHttpSender(config, fac);
            _redis = rcon.RedisConnection;
        }
        public string ReadGroup(string ga, KnxDataPoint dpt)
        {
            //Fall back to http sender for reading
            return _khs.ReadGroup(ga, dpt);
        }

        public void WriteGroup(string gaaddress, string value, KnxDataPoint dpt)
        {
            if (!_sendEnabled) return;

            var dpttosend = dpt.ToString().ToUpper();

            var sub = _redis.GetSubscriber();
            sub.Publish("knx_write_bus", JsonConvert.SerializeObject(new { ga = gaaddress, val = value, dpt = dpttosend }));

        }
    }
}