﻿using System.Collections.Generic;
using System.Net.Http;
using Meeseeks.Server.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Meeseeks.Server.Knx
{
    public class KnxHttpSender : IKnxSender
    {
        private readonly string _watchHost;
        private readonly bool _sendEnabled;
        private readonly ILogger<KnxHttpSender> _logger;

        public KnxHttpSender(IConfigurationRoot config, ILoggerFactory fac)
        {
            _logger = fac.CreateLogger<KnxHttpSender>();
            _sendEnabled = bool.Parse(config["Knx:EnableSend"]);
            _watchHost = config["Knx:WatchHost"];
        }

        public string ReadGroup(string ga, KnxDataPoint dpt)
        {
            using (HttpClient hc = new HttpClient())
            {
                string readJson = hc.GetStringAsync($"http://{_watchHost}/read?addr={ga}").Result;
                var value = JsonConvert.DeserializeObject<Dictionary<string, object>>(readJson)["value"];
                return value.ToString();
            }
        }

        public void WriteGroup(string gaaddress, string value, KnxDataPoint dpt)
        {
            if (!_sendEnabled)
            {
                _logger.LogDebug($"Would send {value} to {gaaddress}");
                return;
            }

            using (HttpClient hc = new HttpClient())
            {
                var dpttosend = dpt.ToString().ToUpper();
                var formattableString = $"http://{_watchHost}/write?addr={gaaddress}&value={value}&dpt={dpttosend}";
                _logger.LogDebug(formattableString);

                hc.GetAsync(formattableString).Wait();
            }
        }
    }
}