﻿namespace Meeseeks.Server.Knx
{
    /// <summary>
    /// Type of the telegram on the Bus
    /// </summary>
    public enum KnxPacketType
    {
        /// <summary>
        /// Read Request
        /// </summary>
        Read,

        /// <summary>
        /// Write Request
        /// </summary>
        Write
    };
}