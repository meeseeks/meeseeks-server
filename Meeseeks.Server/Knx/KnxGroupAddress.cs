﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;

namespace Meeseeks.Server.Knx
{
    [BsonIgnoreExtraElements]
    public class KnxGroupAddress
    {
        [Key]
        [BsonId]
        public string Address { get; set; }

        public string Title { get; set; }

        public string MainTitle { get; set; }

        public string MiddleTitle { get; set; }

        public KnxDataPoint DptFormat { get; set; }

        public bool IsSensor { get; set; }

        public override string ToString()
        {
            return $"{Address} [{MainTitle}->{MiddleTitle}->{Title}]";
        }
    }
}