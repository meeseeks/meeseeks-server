using System;
using System.Text;
using Meeseeks.Server.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Meeseeks.Server.Knx
{
    public class KnxRabbitSender : IKnxSender
    {
        private ILogger<KnxRabbitSender> _logger;
        private readonly bool _sendEnabled;
        private readonly ConnectionFactory _rabbitFactory;

        public KnxRabbitSender(IConfigurationRoot config, ILoggerFactory fac)
        {
            _logger = fac.CreateLogger<KnxRabbitSender>();
            _sendEnabled = bool.Parse(config["Knx:EnableSend"]);
            _rabbitFactory = new ConnectionFactory { HostName = config["rabbithost"] };

        }

        //Read from KNX
        public string ReadGroup(string ga, KnxDataPoint dpt)
        {
            var corrId = Guid.NewGuid().ToString();
            using (var connection = _rabbitFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare("knx_read_request", true, false, false, null);
                string replyQueueName = channel.QueueDeclare($"knx_read_response_srv{Guid.NewGuid()}", true, false, true, null).QueueName;

                byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new { ga = ga, replyTo = replyQueueName }));

                var consumer = new QueueingBasicConsumer(channel);
               
                string consumerId = channel.BasicConsume(queue: replyQueueName,consumer: consumer);

                //Send request
                var props = channel.CreateBasicProperties();
                props.ReplyTo = replyQueueName;
                props.CorrelationId = corrId;
                channel.BasicPublish(exchange: "",
                    routingKey: "knx_read_request",
                    basicProperties: props,
                    body: body);

                DateTime timeout = DateTime.UtcNow.AddSeconds(30);
                while (timeout > DateTime.UtcNow)
                {
                    Console.WriteLine($"Waiting for result on {corrId} on queue {replyQueueName}");
                    BasicDeliverEventArgs ea = consumer.Queue.Dequeue();
                    Console.WriteLine($"got response for {ea.BasicProperties.CorrelationId} on queue {replyQueueName}");
                    if (ea.BasicProperties.CorrelationId == corrId)
                    {
                        channel.BasicCancel(consumerId);
                        string rpcResult = Encoding.UTF8.GetString(ea.Body);
                        Console.WriteLine("KNXSENDER_READ: " + rpcResult);
                        JObject foo = JsonConvert.DeserializeObject<JObject>(rpcResult);
                        return foo["value"].Value<string>();
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// Send a telegram to KNX
        /// </summary>
        /// <param name="gaaddress"></param>
        /// <param name="value"></param>
        /// <param name="dpt"></param>
        public void WriteGroup(string gaaddress, string value, KnxDataPoint dpt)
        {
            if (!_sendEnabled) return;
            using (var connection = _rabbitFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare("knx_write",true,false,false,null);

                string dpttosend = dpt.ToString().ToUpper();
                byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new { ga = gaaddress, val = value, dpt = dpttosend }));

                channel.BasicPublish(exchange: "",
                    routingKey: "knx_write",
                    basicProperties: null,
                    body: body);
            }
        }
    }
}