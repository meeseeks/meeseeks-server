﻿namespace Meeseeks.Server.Knx
{
    /// <summary>
    /// KNX Datapoint Formats
    /// </summary>
    public enum KnxDataPoint
    {
        /// <summary>
        /// Not specified
        /// </summary>
        Notset = 0,

        /// <summary>
        /// Boolean (ON/Off)
        /// </summary>
        Dpt1 = 1,

        /// <summary>
        /// Boolean with Priority
        /// </summary>
        Dpt2 = 2,

        /// <summary>
        /// Character (ASCII)
        /// </summary>
        Dpt4 = 4,

        /// <summary>
        /// 8-bit unsigned (Percentage, Angle)
        /// </summary>
        Dpt5 = 5,

        /// <summary>
        /// 8-bit signed
        /// </summary>
        Dpt6 = 6,

        /// <summary>
        /// 16-bit unsigned
        /// </summary>
        Dpt7 = 7,

        /// <summary>
        /// 16-bit signed
        /// </summary>
        Dpt8 = 8,

        /// <summary>
        /// 16-bit float
        /// </summary>
        Dpt9 = 9,

        /// <summary>
        /// Time
        /// </summary>
        Dpt10 = 10,

        /// <summary>
        /// Date
        /// </summary>
        Dpt11 = 11,

        /// <summary>
        /// 32-bit unsigned
        /// </summary>
        Dpt12 = 12,

        /// <summary>
        /// 32-bit unsigned
        /// </summary>
        Dpt13 = 13,

        /// <summary>
        /// 32-bit Float
        /// </summary>
        Dpt14 = 14,

        /// <summary>
        /// 14-Byte String
        /// </summary>
        Dpt16 = 16
    }
}
