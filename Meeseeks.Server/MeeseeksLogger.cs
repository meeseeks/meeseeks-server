using System;
using Microsoft.ApplicationInsights;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server
{
    public class MeeseeksLogger : ILogger
    {
        private readonly string _cat;
        private readonly object _lock = new object();
        private TelemetryClient _tc;

        public MeeseeksLogger(string categoryName)
        {
            var cat = categoryName.Split('.');
            _cat = cat[cat.Length-1];
            _tc = new TelemetryClient();
        }


        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState,Exception,string> formatter)
        {
            if (_cat == "Kestrel" || _cat == "ControllerActionInvoker" || _cat == "TreeRouter") return;
            string text = formatter(state, exception);

            if (logLevel < LogLevel.Error) return;

            switch (logLevel)
            {
                case LogLevel.Debug:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case LogLevel.Information:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case LogLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogLevel.Critical:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
            }

            lock (_lock)
            {
                string dt = DateTime.Now.ToString("HH:mm:ss");
                Console.Write($"[{dt} | {logLevel.ToString().PadRight(12)} | {_cat.PadRight(25)}]  ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(text);
                Console.ForegroundColor = ConsoleColor.Gray;
                _tc.TrackTrace(text);
            }
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
    }
}