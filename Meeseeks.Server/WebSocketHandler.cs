﻿using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Meeseeks.Server
{
    public class WebSocketHandler : IWebSocketHandler
    {

        private readonly ConcurrentDictionary<int,ConcurrentQueue<WebSocketMessage>> _messageQueue;
        private readonly ILogger<WebSocketHandler> _logger;

        public WebSocketHandler(ILoggerFactory logfac)
        {
            _logger = logfac.CreateLogger<WebSocketHandler>();
            _messageQueue = new ConcurrentDictionary<int, ConcurrentQueue<WebSocketMessage>>();
        }

        public async void HandleRequest(WebSocket webSocket)
        {
            int connId = _messageQueue.Count;
            _messageQueue[connId] = new ConcurrentQueue<WebSocketMessage>();
            _logger.LogDebug($"New Connection #{connId}");
            while (webSocket.State == WebSocketState.Open)
            {

                //See if there is something in our queue
                while (!_messageQueue[connId].IsEmpty)
                {
                    WebSocketMessage nextSockMsg;
                    if (!_messageQueue[connId].TryDequeue(out nextSockMsg)) continue;

                    string reqText = JsonConvert.SerializeObject(nextSockMsg);
                    await SendToSocket(reqText, webSocket);
                }

                //var token = CancellationToken.None;
                //var buffer = new ArraySegment<byte>(new byte[4096]);

                ////Read from websocket
                //var received = await webSocket.ReceiveAsync(buffer, token);
                //if (received.MessageType != WebSocketMessageType.Text) continue;

                //var requestMessage = Encoding.UTF8.GetString(buffer.Array, buffer.Offset, buffer.Count);
                //await SendToSocket(requestMessage, webSocket);
            }
        }

        private async Task SendToSocket(string msg, WebSocket sock)
        {
            var token = CancellationToken.None;
            var type = WebSocketMessageType.Text;
            var data = Encoding.UTF8.GetBytes(msg);
            var buffer = new ArraySegment<byte>(data);
            if (sock.State == WebSocketState.Open)
            {
                await sock.SendAsync(buffer, type, true, token);
            }
        }

        public void SendMessage(WebSocketMessage sockMsg)
        {
            foreach (int connId in _messageQueue.Keys)
            {
                _messageQueue[connId].Enqueue(sockMsg);
            }
        }

    }

}