using System;
using Meeseeks.Server.Knx;

namespace Meeseeks.Server.Models
{
    public class KnxBusEventArgs : EventArgs
    {
        public KnxBusEventArgs(KnxGroupAddress ga, string value)
        {
            GroupAddress = ga;
            Value = value;
        }

        public string Value { get; set; }

        public KnxGroupAddress GroupAddress { get; set; }

        public override string ToString()
        {
            return $"{GroupAddress.Address} --> {Value}";
        }

    }
}