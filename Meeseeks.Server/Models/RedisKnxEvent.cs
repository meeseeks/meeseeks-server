using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class RedisKnxEvent
    {
        [JsonProperty("src")]
        public string Source { get; set; }

        [JsonProperty("dst")]
        public string Destination { get; set; }

        [JsonProperty("dpt")]
        public string DataType { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}