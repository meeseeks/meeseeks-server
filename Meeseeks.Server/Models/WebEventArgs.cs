using System;

namespace Meeseeks.Server.Models
{
    public class WebEventArgs : EventArgs
    {

        public WebEventArgs(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}