using System;
using Meeseeks.Server.Core;

namespace Meeseeks.Server.Models
{
    public class OneWireEventArgs : EventArgs
    {
        public OneWireEventArgs(Sensor sensor, double currentValue)
        {
            Sensor = sensor;
            CurrentValue = currentValue;
        }

        public double CurrentValue { get; set; }

        public Sensor Sensor { get; set; }
    }
}