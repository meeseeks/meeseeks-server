using Meeseeks.Server.Core;

namespace Meeseeks.Server.Models
{
    public class SensorTypeOptions
    {
        public static SensorTypeOptions GetOptions(SensorType sMeasure)
        {

            switch (sMeasure)
            {
                case SensorType.HkStellwert:
                    return new SensorTypeOptions
                    {
                        Unit = "%",
                        AggegateType = AggregateType.Avg
                    };
                case SensorType.Temperature:
                    return new SensorTypeOptions
                    {
                        Unit = "�C",
                        AggegateType = AggregateType.Avg
                    };
                case SensorType.Brightness:
                    return new SensorTypeOptions
                    {
                        Unit = "lux",
                        AggegateType = AggregateType.Avg
                    };
                case SensorType.Power:
                    return new SensorTypeOptions
                    {
                        Unit = "W",
                        AggegateType = AggregateType.Avg
                    };
                case SensorType.Voltage:
                    return new SensorTypeOptions
                    {
                        Unit = "V",
                        AggegateType = AggregateType.Avg
                    };
                case SensorType.Costs:
                    return new SensorTypeOptions
                    {
                        Unit = "�",
                        AggegateType = AggregateType.Avg
                    };

            }
            return new SensorTypeOptions {Unit = "", AggegateType = AggregateType.Avg};
        }

        public AggregateType AggegateType { get; set; }
        public string Unit { get; set; }
    }
}