using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class RedisSpeechEvent
    {
        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("text")]
        public string TextToSpeak { get; set; }
    }
}