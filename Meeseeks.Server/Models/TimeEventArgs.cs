using System;

namespace Meeseeks.Server.Models
{
    public class TimeEventArgs : EventArgs
    {

        public TimeEventArgs(DateTime dt)
        {
            CurrentDateTime = dt;
        }

        public DateTime CurrentDateTime { get; set; }
    }
}