using System;

namespace Meeseeks.Server.Models
{
    public class PlotOptions
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Type { get; set; }
        public string Mode { get; set; }
        public string Resolution { get; set; }
    }
}