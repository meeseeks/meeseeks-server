using System;
using System.Collections.Generic;

namespace Meeseeks.Server.Models
{
    public class PlotByIdRequestModel
    {
        public List<Guid> SensorId { get; set; }
        public PlotOptions Options { get; set; }
    }
}