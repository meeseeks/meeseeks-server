using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class RedisSoundEvent
    {
        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("sound")]
        public string Soundfile { get; set; }
    }
}