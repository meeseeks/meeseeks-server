using System;
using Meeseeks.Server.Core;

namespace Meeseeks.Server.Models
{
    public class PlotRequestModel
    {
        public string Bus { get; set; }
        public string Sensor { get; set; }
        public int Days { get; set; }

        public BusType GetBusType()
        {
            return (BusType) Enum.Parse(typeof(BusType), Bus, true);
        }
    }
}