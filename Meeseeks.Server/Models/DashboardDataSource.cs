using System.Collections.Generic;
using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class DashboardDataSource
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("addr")]
        public string Address { get; set; }
        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("valuemap")]
        public List<ValueMap> ValueMappings { get; set; }

        public DashboardDataSource()
        {
            ValueMappings = new List<ValueMap>();
        }
    }
}