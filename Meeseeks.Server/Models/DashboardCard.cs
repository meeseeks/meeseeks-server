using System.Collections.Generic;
using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class DashboardCard
    {
        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("datasources")]
        public List<DashboardDataSource> DataSources { get; set; }

        public DashboardCard()
        {
            DataSources = new List<DashboardDataSource>();
        }
    }
}