using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class ValueMap
    {
        [JsonProperty("v")]
        public string Value { get; set; }
        [JsonProperty("d")]
        public string DisplayValue { get; set; }
    }
}