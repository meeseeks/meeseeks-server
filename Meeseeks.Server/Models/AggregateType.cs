namespace Meeseeks.Server.Models
{
    public enum AggregateType
    {
        Avg,
        Sum,
        Max,
        Min
    }
}