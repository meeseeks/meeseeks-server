using Newtonsoft.Json;

namespace Meeseeks.Server.Models
{
    public class WebSocketMessage
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("payload")]
        public object Payload { get; set; }
    }
}