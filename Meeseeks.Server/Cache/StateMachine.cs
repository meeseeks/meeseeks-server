using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;

namespace Meeseeks.Server.Cache
{
    public class StateMachine : IStateMachine
    {
        private readonly IStateMachineRepository _smRep;

        public StateMachine() : this(new InMemoryStateMachine())
        {
        }

        public StateMachine(IStateMachineRepository smRep)
        {
            _smRep = smRep;
        }


        /// <summary>
        /// Read a value from the store
        /// </summary>
        /// <param name="bus"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public T ReadState<T>(BusType bus, string address)
        {
            var firstOrDefault = _smRep.Get(address,bus);
            return (T)firstOrDefault?.Value;
        }

        public T ReadState<T>(string key)
        {
            return ReadState<T>(BusType.Manual, key);
        }


        /// <summary>
        /// Write/Add a new state to the hold
        /// </summary>
        /// <param name="address"></param>
        /// <param name="data"></param>
        /// <param name="bus"></param>
        public void WriteState<T>(BusType bus, string address, T data)
        {
            State newState = new State
            {
                Address = address,
                Bus = bus,
                Value = data
            };
            _smRep.Set(newState);

        }

        public void WriteState<T>(string key, T data)
        {
            WriteState(BusType.Manual, key, data);
        }
    }
}