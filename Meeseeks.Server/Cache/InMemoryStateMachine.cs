﻿using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace Meeseeks.Server.Cache
{
    public class InMemoryStateMachine : IStateMachineRepository
    {
        private readonly MemoryCache _cache;

        private string BuildKey(string key, BusType type)
        {
            return $"{type}_{key}";
        }

        private string BuildKey(State s)
        {
            return $"{s.Bus}_{s.Address}";
        }

        public InMemoryStateMachine()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public State Get(string key)
        {
            string cachekey = BuildKey(key, BusType.Manual);
            return _cache.Get<State>(cachekey);
        }
        public State Get(string key, BusType bus)
        {
            string cachekey = BuildKey(key, bus);
            return _cache.Get<State>(cachekey);
        }

        public void Delete(State state)
        {

            _cache.Remove(BuildKey(state));
        }

        public void Set(State state)
        {
            _cache.Set(BuildKey(state), state);
        }
    }
}