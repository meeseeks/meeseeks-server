using Meeseeks.Server.Core;

namespace Meeseeks.Server.Cache
{
    public class State
    {
        public BusType Bus;
        public string Address;
        public object Value;
    }
}