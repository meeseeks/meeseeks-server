using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Meeseeks.Server.Cache
{
    public class RedisStateMachine : IStateMachine
    {
        private readonly IDatabase _redis;

        public RedisStateMachine(IRedisConnection redis)
        {
            _redis = redis.RedisConnection.GetDatabase();
        }

        public T ReadState<T>(BusType bus, string address)
        {
            return ReadState<T>($"{bus}_{address}");
        }

        public T ReadState<T>(string key)
        {
            string redisValue = _redis.StringGet(key);
            return redisValue == null ? default(T) : JsonConvert.DeserializeObject<T>(redisValue);
        }

        public void WriteState<T>(BusType bus, string address, T data)
        {
            WriteState($"{bus}_{address}",data);
        }

        public void WriteState<T>(string key, T data)
        {
            _redis.StringSet(key, JsonConvert.SerializeObject(data));
        }
    }
}