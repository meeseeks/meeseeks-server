using System;
using System.Collections.Generic;
using SharpGoogleCharts;

namespace Meeseeks.Server.Plot
{
    public class PlotResolutionInfo
    {
        public PlotResolution Resolution { get; }
        public List<DateTime> ExpectedXValues { get; }
        public string MongoGroupBucket { get; }

        public PlotResolutionInfo(PlotResolution res, DateTime start, DateTime end)
        {
            Resolution = res;
            ExpectedXValues = new List<DateTime>();

            //determine XValues based on resolution
            switch (res)
            {
                case PlotResolution.Minute:
                    ExpectedXValues = MinuteValues(start, end);
                    MongoGroupBucket = "day:{$dayOfYear: '$RecordTime'},minute:{$minute: '$RecordTime'}";
                    break;
                case PlotResolution.Hour:
                    ExpectedXValues = HourValues(start, end);
                    MongoGroupBucket = "day:{$dayOfYear: '$RecordTime'},hour:{$hour: '$RecordTime'}";
                    break;
                case PlotResolution.Day:
                    ExpectedXValues = DayValues(start, end);
                    MongoGroupBucket = "$dayOfYear: '$RecordTime'";
                    break;
                case PlotResolution.Week:
                    ExpectedXValues = WeekValues(start, end);
                    MongoGroupBucket = "$week: '$RecordTime'";
                    break;
                case PlotResolution.Month:
                    ExpectedXValues = MonthValues(start, end);
                    MongoGroupBucket = "$month: '$RecordTime'";
                    break;
                case PlotResolution.Year:
                    ExpectedXValues = YearValues(start, end);
                    MongoGroupBucket = "$year: '$RecordTime'";
                    break;
                case PlotResolution.None:
                    MongoGroupBucket = "1";
                    break;

            }
        }

        public DateTime RoundDateTime(DateTime dt)
        {
            switch (Resolution)
            {
                case PlotResolution.Minute:
                    return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0);
                case PlotResolution.Hour:
                    return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0);
                case PlotResolution.Day:
                    return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
                case PlotResolution.Month:
                    return new DateTime(dt.Year, dt.Month, 1, 0, 0, 0);
                case PlotResolution.Year:
                    return new DateTime(dt.Year, 1, 1, 0, 0, 0);
                case PlotResolution.Week:
                    dt = dt.Date;
                    while (dt.DayOfWeek != DayOfWeek.Monday)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                default:
                    return dt;
            }
        }

        private List<DateTime> YearValues(DateTime start, DateTime end)
        {
            List<DateTime> dtl = new List<DateTime>();
            for (int i = start.Year; i <= end.Year; i++)
                dtl.Add(new DateTime(i, 1, 1, 0, 0, 0));
            return dtl;
        }

       
        private List<DateTime> MonthValues(DateTime start, DateTime end)
        {
            List<DateTime> dtl = new List<DateTime>();
            var runDate = start;
            while (runDate < end)
            {
                dtl.Add(new DateTime(runDate.Year, runDate.Month, 1));
                runDate = runDate.AddMonths(1);
            }
            return dtl;
        }

        private List<DateTime> WeekValues(DateTime start, DateTime end)
        {
            List<DateTime> dtl = new List<DateTime>();
            var runDate = start.Date;
            while (runDate < end)
            {
                if (runDate.DayOfWeek == DayOfWeek.Monday)
                {
                    dtl.Add(new DateTime(runDate.Year, runDate.Month, runDate.Day));
                }
                runDate = runDate.AddDays(1);
            }
            return dtl;
        }

        private List<DateTime> MinuteValues(DateTime start, DateTime end)
        {
            List<DateTime> dtl = new List<DateTime>();
            var runDate = start;
            while (runDate < end)
            {
                dtl.Add(new DateTime(runDate.Year, runDate.Month, runDate.Day, runDate.Hour, runDate.Minute, 0));
                runDate = runDate.AddMinutes(1);
            }
            return dtl;
        }

        private List<DateTime> HourValues(DateTime start, DateTime end)
        {
            List<DateTime> dtl = new List<DateTime>();
            var runDate = start;
            while (runDate < end)
            {
                dtl.Add(new DateTime(runDate.Year, runDate.Month, runDate.Day, runDate.Hour, 0, 0));
                runDate = runDate.AddHours(1);
            }
            return dtl;
        }

        private List<DateTime> DayValues(DateTime start, DateTime end)
        {
            List<DateTime> dtl = new List<DateTime>();
            var runDate = start;
            while (runDate < end)
            {
                dtl.Add(new DateTime(runDate.Year, runDate.Month, runDate.Day));
                runDate = runDate.AddDays(1);
            }
            return dtl;
        }


        public string ApplyDateBucketToQuery(string aggregationJson)
        {
            string datebucket = $"{{ {MongoGroupBucket} }}";
            if (Resolution == PlotResolution.None)
                datebucket = "'1'";
            string aggegationJson = aggregationJson.Replace("\"DATEBUCKET\"", datebucket);
            return aggegationJson;
        }
    }
}