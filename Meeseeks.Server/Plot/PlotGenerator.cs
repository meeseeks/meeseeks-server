﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using SharpGoogleCharts;

namespace Meeseeks.Server.Plot
{
    public class PlotGenerator
    {
        public Task<List<BsonDocument>> RunAggregationPipelineAsync<T>(string aggegationJson, IMongoCollection<T> dataSource, FilterDefinition<T> mongoFilter)
        {
            //Create aggregation
            BsonArray stages = BsonSerializer.Deserialize<BsonArray>(aggegationJson);
            var baseAgg = dataSource.Aggregate().Match(mongoFilter);
            IAggregateFluent<BsonDocument> aggregation = null;
            foreach (BsonValue t in stages)
            {
                aggregation = aggregation == null ? baseAgg.AppendStage<BsonDocument>((BsonDocument)t) : aggregation.AppendStage<BsonDocument>((BsonDocument)t);
            }
            return aggregation.ToListAsync();
        }

        

        public ChartOptions ConvertPlotOptions(PlotOptions opt)
        {
            ChartOptions chartOptions = new ChartOptions
            {
                ChartType = PlotChartType.Line,
                Showgrid = true,
                XAxis = { Datatype = AxisDataType.DateTime, Label = "Datum" },
                YAxis = { Datatype = AxisDataType.Numeric, Label = "Kosten" },
                Showlegend = false
            };

            switch (opt.Resolution)
            {
                case "none":
                    chartOptions.Resolution = PlotResolution.None;
                    break;
                case "minutes":
                    chartOptions.Resolution = PlotResolution.Minute;
                    break;
                case "hours":
                    chartOptions.Resolution = PlotResolution.Hour;
                    break;
                case "days":
                    chartOptions.Resolution = PlotResolution.Day;
                    break;
                case "weeks":
                    chartOptions.Resolution = PlotResolution.Week;
                    break;
                case "years":
                    chartOptions.Resolution = PlotResolution.Year;
                    break;
            }

            switch (opt.Type)
            {
                case "pie":
                    chartOptions.ChartType = PlotChartType.Pie;
                    break;
                case "donut":
                    chartOptions.ChartType = PlotChartType.Donut;
                    break;
                case "bar":
                    chartOptions.ChartType = PlotChartType.Bar;
                    break;
                case "line":
                    chartOptions.ChartType = PlotChartType.Line;
                    break;
            }
            return chartOptions;
        }


    }
}