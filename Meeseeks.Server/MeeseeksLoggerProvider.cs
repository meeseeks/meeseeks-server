using Microsoft.Extensions.Logging;

namespace Meeseeks.Server
{
    public class MeeseeksLoggerProvider : ILoggerProvider
    {
        public void Dispose()
        {

        }

        public ILogger CreateLogger(string categoryName)
        {
            return new MeeseeksLogger(categoryName);
        }
    }
}