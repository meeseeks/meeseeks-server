﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Meeseeks.Server.Blockly;
using Meeseeks.Server.Interfaces;

namespace Meeseeks.Server.Data.Static
{
    public class StaticBlocklyRepository : IBlocklyRepository
    {

        private readonly List<BlocklyBlock> _blocks;

        public StaticBlocklyRepository()
        {
            _blocks = new List<BlocklyBlock>();

            //Fill in some static blocks
            BlocklyBlock bb = new BlocklyBlock
            {
                Name = "knx_switch",
                Tooltip = "foobar",
                HelpUrl = "google.de",
                ColorHue = 100,
                TextTemplate = "Foo %1 Bar %2",
                Arguments = new List<BlocklyArgument>
                {
                    new BlocklyArgument
                    {
                        Type = BlocklyInputType.Field,
                        ValueName = "foovalue1"
                    },
                    new BlocklyArgument
                    {
                        Type = BlocklyInputType.Dropdown,
                        ValueName = "bardrop",
                        Options = new List<string[]>
                        {
                            new [] {"opt1","OPTION1"},
                            new [] {"opt2","OPTION2"}
                        }
                    }
                }
            };
            _blocks.Add(bb);

        }

        public Task<List<BlocklyBlock>> FetchAll()
        {
            return Task.FromResult(_blocks.ToList());
        }

        public Task<BlocklyBlock> Find(Expression<Func<BlocklyBlock, bool>> predicate)
        {
            return Task.FromResult(_blocks.Where(predicate.Compile()).FirstOrDefault());
        }
    }
}
