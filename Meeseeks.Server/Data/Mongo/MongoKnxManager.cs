using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Meeseeks.Server.Data.Mongo
{
    public class MongoKnxManager : MongoBase, IKnxManager
    {
        private readonly ILogger<MongoKnxManager> _logger;

        public MongoKnxManager(IConfigurationRoot config, ILoggerFactory logfac) : base(config)
        {
            _logger = logfac.CreateLogger<MongoKnxManager>();
        }

        public async Task<KnxGroupAddress> FindGroupAddress(string address)
        {
            IMongoCollection<KnxGroupAddress> mc = ConnectMongo<KnxGroupAddress>("knx", "groupaddresses");
            var ga = await mc.Find(x => x.Address == address).FirstOrDefaultAsync();
            if (ga != null) return ga;

            _logger.LogInformation($"Creating GA {address} in database");
            //create new GA if not found in DB
            KnxGroupAddress newga = new KnxGroupAddress
            {
                Address = address,
                Title = "",
                MainTitle = "",
                MiddleTitle = "",
                DptFormat = KnxDataPoint.Notset,
                IsSensor = false
            };
            await mc.InsertOneAsync(newga);
            return newga;
        }

        public async void UpdateGa(KnxGroupAddress ga)
        {
            IMongoCollection<KnxGroupAddress> mc = ConnectMongo<KnxGroupAddress>("knx", "groupaddresses");
            await mc.ReplaceOneAsync(x => x.Address == ga.Address, ga);
        }

        public async Task<IEnumerable<KnxGroupAddress>> ListAllGroupAdresses()
        {
            IMongoCollection<KnxGroupAddress> mc = ConnectMongo<KnxGroupAddress>("knx", "groupaddresses");
            return await mc.Find(FilterDefinition<KnxGroupAddress>.Empty).ToListAsync();
        }
    }
}