using System;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Meeseeks.Server.Data.Mongo
{
    public abstract class MongoBase
    {
        private readonly string _mongoUrl;
        private readonly string _mongoDatabase;

        protected MongoBase(IConfigurationRoot config)
        {
            _mongoUrl = config["mongo:url"];
            _mongoDatabase = config["mongo:database"];
        }
        protected IMongoCollection<T> ConnectMongo<T>(string collection)
        {
            return ConnectMongo<T>(null, collection);
        }

        protected IMongoCollection<T> ConnectMongo<T>(string schema, string collection)
        {
            try
            {
                var mclient = new MongoClient(_mongoUrl);
                var db = mclient.GetDatabase(_mongoDatabase);
                string dbname = collection;
                if (!String.IsNullOrEmpty(schema))
                    dbname = schema + "_" + collection;

                return db.GetCollection<T>(dbname);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't connect to mongo", ex);
            }
        }
    }
}