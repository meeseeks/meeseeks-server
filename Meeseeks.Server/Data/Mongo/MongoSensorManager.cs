using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Models;
using Meeseeks.Server.Plot;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Newtonsoft.Json;
using RabbitMQ.Client;
using SharpGoogleCharts;

namespace Meeseeks.Server.Data.Mongo
{



    public class MongoSensorManager : MongoBase, ISensorManager
    {
        private readonly ILogger<MongoSensorManager> _logger;
        private readonly ConnectionFactory _rabbitFactory;
        private const string DbSchema = "sensors";
        public MongoSensorManager(IConfigurationRoot config, ILoggerFactory logfac) : base(config)
        {
            _logger = logfac.CreateLogger<MongoSensorManager>();
            _rabbitFactory = new ConnectionFactory { HostName = config["rabbithost"] };
        }
        public async void RecordValueAsync(BusType bus, string deviceAddress, double currentValue)
        {
            var s = await GetSensorAsync(bus, deviceAddress) ?? CreateSensor(bus, deviceAddress);

            //var mc = ConnectMongo<SensorValue>(DbSchema, "data");

            //await mc.InsertOneAsync(new SensorValue
            //{
            //    RecordTime = DateTime.UtcNow,
            //    Value = currentValue,
            //    SensorId = s.SensorId
            //});

            //send to redis
            //var sub = _redis.RedisConnection.GetSubscriber();
            //await sub.PublishAsync("sensor_readings", serializedSensorReading);

            //_redis.RedisConnection.GetDatabase(1).StringSet(s.SensorId.ToString(), currentValue);


            string serializedSensorReading = JsonConvert.SerializeObject(new { sid = s.SensorId, value = currentValue, bus = bus.ToString(), type = s.Measure.ToString(), name = s.Description, room = s.Room });

            using (var connection = _rabbitFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("sensors","fanout",durable:true);
    
                var body = Encoding.UTF8.GetBytes(serializedSensorReading);
                
                channel.BasicPublish(exchange: "sensors",
                    routingKey: "",
                    basicProperties: null,
                    body: body);
            }


            //_logger.LogInformation($"Record Sensor: {bus}.{deviceAddress} => {currentValue}");
        }

        public async Task<Sensor> GetSensorAsync(BusType bus, string device)
        {
            var mc = ConnectMongo<Sensor>(DbSchema, "devices");
            return await mc.Find(x => x.Bus == bus && x.Address == device).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<SensorValue>> GetSensorValuesAsync(BusType bus, string device, DateTime start, DateTime end)
        {
            var s = await GetSensorAsync(bus, device);
            var mc = ConnectMongo<SensorValue>(DbSchema, "data");
            return await mc.Find(x => x.SensorId == s.SensorId && x.RecordTime >= start && x.RecordTime <= end).ToListAsync();
        }

        public async Task<IEnumerable<SensorValue>> GetSensorValuesAsync(Guid sensorid, DateTime start, DateTime end)
        {

            var mc = ConnectMongo<SensorValue>(DbSchema, "data");
            return await mc.Find(x => x.SensorId == sensorid && x.RecordTime >= start && x.RecordTime <= end).ToListAsync();
        }


        public async Task<List<PlotPoint>> AggregateForPlotAsync(Guid sensorid, PlotOptions opts)
        {
            //Prepare aggregation command
            PlotGenerator gen = new PlotGenerator();
            var copts = gen.ConvertPlotOptions(opts);
            PlotResolutionInfo pri = new PlotResolutionInfo(copts.Resolution, opts.StartDate, opts.EndDate);
            string plotAggregation = File.ReadAllText("Plot/PlotQuery.txt");
            string aggregationJson = pri.ApplyDateBucketToQuery(plotAggregation);

            //Prepare DB call
            var mc = ConnectMongo<SensorValue>(DbSchema, "data");
            var fb = Builders<SensorValue>.Filter;
            var filter = fb.And(
                fb.Eq(x => x.SensorId, sensorid), 
                fb.Gte(x => x.RecordTime, opts.StartDate),
                fb.Lte(x => x.RecordTime, opts.EndDate));

            //Execute
            var result = await gen.RunAggregationPipelineAsync(aggregationJson, mc, filter);

            //Process            
            var plotPoints = new List<PlotPoint>();

            foreach (var bd in result)
            {
                DateTime dt = pri.RoundDateTime(bd["date"].ToUniversalTime());
                double sum = bd["sensorSum"].ToDouble();
                double min = bd["sensorMin"].ToDouble();
                double max = bd["sensorMax"].ToDouble();
                double avg = bd["sensorAvg"].ToDouble();
                plotPoints.Add(new PlotPoint(dt, avg));
            }

            //Fill in expected times
            foreach (DateTime dtExpected in pri.ExpectedXValues)
            {
                if (!plotPoints.Exists(x => (DateTime)x.X == dtExpected))
                {
                    plotPoints.Add(new PlotPoint(dtExpected, 0.0));
                }
            }

            return plotPoints.OrderBy(x => (DateTime)x.X).ToList();
        }

        public void EnsureSensorsCreated(BusType bus, List<string> devices)
        {
            foreach (string dev in devices)
            {
                CreateSensor(bus, dev);
            }
        }

        private Sensor CreateSensor(BusType bus, string dev)
        {
            var mc = ConnectMongo<Sensor>(DbSchema, "devices");

            var hasSensor = mc.Find(x => x.Address == dev).FirstOrDefault();
            if (hasSensor != null) return hasSensor;

            //create sensor as it dosn't exist
            Sensor newsensor = new Sensor
            {
                SensorId = Guid.NewGuid(),
                Address = dev,
                Bus = bus,
                IsRegistered = false,
                Measure = SensorType.Unknown
            };
            mc.InsertOne(newsensor);
            _logger.LogInformation($"Created sensor in database {dev}");
            return newsensor;
        }

        public async Task<SensorValue> GetCurrentValueAsync(BusType bus, string device)
        {
            var s = await GetSensorAsync(bus, device);
            if (s == null) return null;
            var mc = ConnectMongo<SensorValue>(DbSchema, "data");
            var sort = Builders<SensorValue>.Sort.Descending(x => x.RecordTime);
            return await mc.Find(x => x.SensorId == s.SensorId).Sort(sort).FirstOrDefaultAsync();
        }

        public Task<List<Sensor>> GetSensors()
        {
            var mc = ConnectMongo<Sensor>(DbSchema, "devices");
            return mc.Find(FilterDefinition<Sensor>.Empty).ToListAsync();
        }

        public async Task<Sensor> GetSensorAsync(Guid sensorId)
        {
            var mc = ConnectMongo<Sensor>(DbSchema, "devices");
            return await mc.Find(x => x.SensorId == sensorId).FirstOrDefaultAsync();
        }

        public void SaveSensor(Sensor sensor)
        {
            var mc = ConnectMongo<Sensor>(DbSchema, "devices");
            mc.ReplaceOne(x => x.SensorId == sensor.SensorId, sensor, new UpdateOptions { IsUpsert = true });

        }
    }
}