using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Meeseeks.Server.Blockly;
using Meeseeks.Server.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Meeseeks.Server.Data.Mongo
{
    public class MongoBlocklyRepository : MongoBase, IBlocklyRepository
    {
        private readonly ILogger<MongoBlocklyRepository> _logger;
        private const string Schema = "blockly";

        public MongoBlocklyRepository(IConfigurationRoot config,ILoggerFactory logfac) : base(config)
        {
            _logger = logfac.CreateLogger<MongoBlocklyRepository>();
        }

        public async Task<List<BlocklyBlock>> FetchAll()
        {
            _logger.LogDebug("Fetching all blocks");
            IMongoCollection<BlocklyBlock> mc = ConnectMongo<BlocklyBlock>(Schema, "blocks");
            return await mc.Find(FilterDefinition<BlocklyBlock>.Empty).ToListAsync();

        }

        public async Task<BlocklyBlock> Find(Expression<Func<BlocklyBlock, bool>> expression)
        {
            _logger.LogDebug("Finding block");
            IMongoCollection<BlocklyBlock> mc = ConnectMongo<BlocklyBlock>(Schema, "blocks");
            return await mc.Find(expression).FirstAsync();
        }
    }
}