using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharpGoogleCharts;

namespace Meeseeks.Server.Data.Mongo
{
    public class InfluxSensorDataProvider : ISensorManager
    {
        private readonly MongoSensorManager _msm;
        private readonly string _hostname;

        public InfluxSensorDataProvider(IConfigurationRoot config, ILoggerFactory logfac)
        {
            _hostname = config["influxdbhost"];
            _msm = new MongoSensorManager(config, logfac);
        }


        public async Task<IEnumerable<SensorValue>> GetSensorValuesAsync(BusType oneWire, string device, DateTime start, DateTime end)
        {
            var sensor = await GetSensorAsync(oneWire, device);
            return await GetSensorValuesAsync(sensor.SensorId, start, end);
        }

        public async Task<IEnumerable<SensorValue>> GetSensorValuesAsync(Guid sensorid, DateTime start, DateTime end)
        {
            string query = $"SELECT mean(\"value\") as v1 FROM \"sensors\" WHERE \"id\" = '{sensorid}' AND time > '{start:O}' AND time < '{end:O}' GROUP BY time(1m) fill(linear)";
            var values = await QueryInfluxAsync(query);
            return values;

        }
        public async Task<SensorValue> GetCurrentValueAsync(BusType oneWire, string vorlaufSensor)
        {
            var sensor = await GetSensorAsync(oneWire, vorlaufSensor);
            var values = await QueryInfluxAsync($"SELECT last(\"value\") as v1 FROM \"sensors\" WHERE \"id\" = '{sensor.SensorId}'");

            return values.FirstOrDefault();

        }
        public Task<List<PlotPoint>> AggregateForPlotAsync(Guid sensorid, PlotOptions opts)
        {
            throw new NotImplementedException();
        }

        private async Task<List<SensorValue>> QueryInfluxAsync(string query)
        {
            List<SensorValue> sensorValues = new List<SensorValue>();
            string url = $"http://{_hostname}/query";
            string jsonResponse;
            using (var client = new HttpClient())
            {

                var postBody = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    {"pretty","true"},
                    {"q",query },
                    {"db","meeseeks" }
                });

                var response = await client.PostAsync(url, postBody);
                jsonResponse = await response.Content.ReadAsStringAsync();
            }

            JArray results = (JArray)JsonConvert.DeserializeObject<JObject>(jsonResponse)["results"];

            if (results.Count == 0)
            {
                return null;
            }

            JObject seriesData = (JObject)((JObject)results[0])["series"][0];

            foreach (JToken dp in (JArray)seriesData["values"])
            {
                var valueArray = dp as JArray;
                if (valueArray == null)
                {
                    continue;
                }

                string datetime = valueArray[0].Value<string>();
                double value = valueArray[1].Value<double>();

                sensorValues.Add(new SensorValue
                {
                    RecordTime = DateTime.Parse(datetime),
                    Value = value
                });
            }

            return sensorValues;
        }

        //Pass-though recording and management to mongo
        public void RecordValueAsync(BusType bus, string deviceAddress, double currentValue)
        {
            _msm.RecordValueAsync(bus, deviceAddress, currentValue);
        }

        public Task<Sensor> GetSensorAsync(BusType bus, string device)
        {
            return _msm.GetSensorAsync(bus, device);
        }

        public void EnsureSensorsCreated(BusType bus, List<string> devices)
        {
            _msm.EnsureSensorsCreated(bus, devices);
        }

        public Task<List<Sensor>> GetSensors()
        {
            return _msm.GetSensors();
        }

        public Task<Sensor> GetSensorAsync(Guid sensorId)
        {
            return _msm.GetSensorAsync(sensorId);
        }

        public void SaveSensor(Sensor sensor)
        {
            _msm.SaveSensor(sensor);
        }


    }
}