@echo off

git tag %1
git push --all

echo Publish project...
dotnet restore
dotnet publish -c Release

echo Building image...
docker rmi registry.gitlab.com/meeseeks/meeseeks-server:latest -f

docker build -t registry.gitlab.com/meeseeks/meeseeks-server:latest .
docker tag registry.gitlab.com/meeseeks/meeseeks-server:latest registry.gitlab.com/meeseeks/meeseeks-server:%1

docker push registry.gitlab.com/meeseeks/meeseeks-server:latest
docker push registry.gitlab.com/meeseeks/meeseeks-server:%1


