﻿using System;
using System.Net.WebSockets;
using Meeseeks.Server.Interfaces;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();


            TelemetryConfiguration.Active.InstrumentationKey = Configuration["ApplicationInsights.InstrumentationKey"];
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            return Program.ConfigureServices(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,IWebSocketHandler wsHandler)
        {
            loggerFactory.AddProvider(new MeeseeksLoggerProvider());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Dashboard/Error");
            }

            app.UseCors(builder =>
            {
                builder.WithOrigins(
                    "http://localhost:4200",
                    "http://meeseeks.keil-connect.com",
                    "https://meeseeks.keil-connect.com"
                    );
            });

            app.UseStaticFiles();
            app.UseWebSockets();

            app.Use(async (http, next) =>
            {
                if (http.WebSockets.IsWebSocketRequest)
                {
                    WebSocket webSocket = await http.WebSockets.AcceptWebSocketAsync();
                    wsHandler.HandleRequest(webSocket);
                }
                else
                {
                    await next();
                }
            });
            
            
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Dashboard}/{action=Index}/{id?}");
            });

            //app.UseSignalR();
        }
    }
}
