﻿using System;
using System.IO;
using Meeseeks.Server.Cache;
using Meeseeks.Server.Core;
using Meeseeks.Server.Data.Mongo;
using Meeseeks.Server.Data.Static;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseApplicationInsights()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .UseUrls("http://*:5050")
                .Build();

            ServerApp srvapp = new ServerApp(Services);
            srvapp.RunServer();

            Console.WriteLine("Press <STRG-C> to exit.");
            host.Run();
        }
        private static IServiceProvider Services { get; set; }

        public static IServiceProvider ConfigureServices(IServiceCollection serviceCollection)
        {


            //serviceCollection.AddTransient<IKnxSender, KnxHttpSender>();
            serviceCollection.AddTransient<IKnxSender, KnxRabbitSender>();

            //Mongo
            serviceCollection.AddTransient<IKnxManager, MongoKnxManager>();
            serviceCollection.AddTransient<ISensorManager, InfluxSensorDataProvider>();
            serviceCollection.AddSingleton<ILogicEngine, LogicEngine>();
            serviceCollection.AddSingleton<IStateMachine, RedisStateMachine>();

            serviceCollection.AddSingleton<IWebSocketHandler, WebSocketHandler>();
            serviceCollection.AddSingleton<IRedisConnection, RedisConnectionProvider>();

            serviceCollection.AddSingleton<IBlocklyRepository, StaticBlocklyRepository>();


            serviceCollection.AddSingleton(BuildConfig());
            serviceCollection.AddMvc();
            serviceCollection.AddCors();

            Services = serviceCollection.BuildServiceProvider();
            return Services;
        }

        private static IConfigurationRoot BuildConfig()
        {
            string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (string.IsNullOrEmpty(env))
                env = "dev";
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env}.json", optional: true)
                .AddEnvironmentVariables();
            return builder.Build();
        }
    }
}
