using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Models;
using SharpGoogleCharts;

namespace Meeseeks.Server.Interfaces
{
    public interface ISensorManager
    {
        void RecordValueAsync(BusType bus, string deviceAddress, double currentValue);
        Task<Sensor> GetSensorAsync(BusType bus, string device);
        Task<IEnumerable<SensorValue>> GetSensorValuesAsync(BusType oneWire, string device, DateTime start, DateTime end);
        Task<IEnumerable<SensorValue>> GetSensorValuesAsync(Guid sensorid, DateTime start, DateTime end);
        void EnsureSensorsCreated(BusType bus,List<string> devices);
        Task<SensorValue> GetCurrentValueAsync(BusType oneWire, string vorlaufSensor);
        Task<List<Sensor>>  GetSensors();
        Task<Sensor> GetSensorAsync(Guid sensorId);
        void SaveSensor(Sensor sensor);
        Task<List<PlotPoint>> AggregateForPlotAsync(Guid sensorid, PlotOptions opts);
    }
}