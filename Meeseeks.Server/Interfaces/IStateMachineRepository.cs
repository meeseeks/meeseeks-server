using Meeseeks.Server.Cache;
using Meeseeks.Server.Core;

namespace Meeseeks.Server.Interfaces
{
    public interface IStateMachineRepository
    {
        State Get(string key);
        State Get(string key, BusType bus);
        void Delete(State state);
        void Set(State state);
    }
}