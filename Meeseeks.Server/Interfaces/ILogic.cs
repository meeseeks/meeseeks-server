﻿using System;

namespace Meeseeks.Server.Interfaces
{
    public interface ILogic
    {
        bool RunListener(EventArgs args);
        string Name { get; }
    }
}