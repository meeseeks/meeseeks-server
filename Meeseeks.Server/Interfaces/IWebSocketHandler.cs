﻿using System.Net.WebSockets;
using Meeseeks.Server.Models;

namespace Meeseeks.Server.Interfaces
{
    public interface IWebSocketHandler
    {
        void HandleRequest(WebSocket webSocket);
        void SendMessage(WebSocketMessage sockMsg);
    }
}