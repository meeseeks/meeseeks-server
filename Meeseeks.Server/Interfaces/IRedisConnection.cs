﻿using StackExchange.Redis;

namespace Meeseeks.Server.Interfaces
{
    public interface IRedisConnection
    {
        ConnectionMultiplexer RedisConnection { get; }
    }
}