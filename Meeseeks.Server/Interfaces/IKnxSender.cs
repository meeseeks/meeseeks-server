﻿using Meeseeks.Server.Knx;

namespace Meeseeks.Server.Interfaces
{
    public interface IKnxSender
    {
        string ReadGroup(string ga, KnxDataPoint dpt);
        void WriteGroup(string gaaddress, string value, KnxDataPoint dpt);
    }
}