using Meeseeks.Server.Core;

namespace Meeseeks.Server.Interfaces
{
    public interface IStateMachine
    {
        /// <summary>
        /// Read a value from the store
        /// </summary>
        /// <param name="bus"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        T ReadState<T>(BusType bus, string address);

        T ReadState<T>(string key);

        /// <summary>
        /// Write/Add a new state to the hold
        /// </summary>
        /// <param name="address"></param>
        /// <param name="data"></param>
        /// <param name="bus"></param>
        void WriteState<T>(BusType bus, string address, T data);

        void WriteState<T>(string key, T data);
    }
}