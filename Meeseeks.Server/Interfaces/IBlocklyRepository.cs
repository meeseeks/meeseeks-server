﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Meeseeks.Server.Blockly;

namespace Meeseeks.Server.Interfaces
{
    public interface IBlocklyRepository
    {
        Task<List<BlocklyBlock>> FetchAll();

        Task<BlocklyBlock> Find(Expression<Func<BlocklyBlock, bool>> predicate);
    }
}