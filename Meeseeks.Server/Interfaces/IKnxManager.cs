using System.Collections.Generic;
using System.Threading.Tasks;
using Meeseeks.Server.Knx;

namespace Meeseeks.Server.Interfaces
{
    public interface IKnxManager
    {
        Task<KnxGroupAddress> FindGroupAddress(string gaval);
        void UpdateGa(KnxGroupAddress ga);

        Task<IEnumerable<KnxGroupAddress>> ListAllGroupAdresses();
    }
}