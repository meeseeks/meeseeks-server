﻿using System;

namespace Meeseeks.Server.Interfaces
{
    public interface ILogicEngine
    {
        void ProcessEvent(EventArgs args);
        void RegisterLogic(ILogic logic);
    }
}