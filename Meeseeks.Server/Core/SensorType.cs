﻿namespace Meeseeks.Server.Core
{
    public enum SensorType
    {
        Unknown = 0,
        Temperature = 1,
        Voltage = 2,
        Power = 3,
        Brightness = 4,
        HkStellwert = 5,
        Costs = 6,
        Current = 7,
        DimValue = 8,
        SwitchState = 9,
        WindowState = 10
    }
}