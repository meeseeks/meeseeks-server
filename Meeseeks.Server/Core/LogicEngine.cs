﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Meeseeks.Server.Interfaces;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Meeseeks.Server.Core
{
    public class LogicEngine: ILogicEngine
    {
        private readonly List<ILogic> _registeredLogics;
        private readonly ILogger<LogicEngine> _logger;
        private TelemetryClient _tc;

        /// <summary>
        /// Init the LogicEngine
        /// </summary>
        public LogicEngine(ILoggerFactory logfac,IServiceProvider sp)
        {
            _registeredLogics = new List<ILogic>();
            _logger = logfac.CreateLogger<LogicEngine>();
            _tc = new TelemetryClient();
            var type = typeof(ILogic);
            var types =
                Assembly.GetEntryAssembly().DefinedTypes.Where(p => !p.IsInterface && type.IsAssignableFrom(p.AsType()));

            foreach (var ti in types)
            {
                ILogic logic = (ILogic)Activator.CreateInstance(ti.AsType(),sp);
                RegisterLogic(logic);
            }

        }

        /// <summary>
        /// Register an Logic. After registration logic will be triggered on base trigger level
        /// </summary>
        /// <param name="logic">LogicListener to be called</param>
        public void RegisterLogic(ILogic logic)
        {
            _logger.LogInformation($"Registered logic: {logic.GetType().Name}");
            _tc.TrackTrace($"Registered logic: {logic.GetType().Name}");
            _registeredLogics.Add(logic);

        }

        /// <summary>
        /// Processes an LogicEvent throught the LogicEngine (gets fired inside the Server Application)
        /// </summary>
        /// <param name="args">Data structure of the event to process</param>
        public void ProcessEvent(EventArgs args)
        {
            
            using (var operation = _tc.StartOperation<RequestTelemetry>("processLogicEvent"))
            {

                _tc.TrackEvent("LogicProcessEvent", new Dictionary<string, string>
                {
                    {"eventargs", JsonConvert.SerializeObject(args)}
                });

                foreach (var logic in _registeredLogics)
                {
                    var innerArgs = args;
                    Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            bool wasRun = logic.RunListener(innerArgs);
                            if (wasRun)
                            {
                                _tc.TrackTrace($"Logic {logic.Name} got executed.");
                            }
                        }
                        catch (Exception e)
                        {
                            _tc.TrackTrace($"Error in listener {logic.Name} occured");
                            _tc.TrackException(e);
                        }
                    });
                }
                _tc.StopOperation(operation);
            }
        }
    }
}