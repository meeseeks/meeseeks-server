﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Meeseeks.Server.Core
{
    [BsonIgnoreExtraElements]
    public class SensorValue
    {
        public Guid SensorId { get; set; }
        public DateTime RecordTime { get; set; }
        public double Value { get; set; }
    }

    public class RangePlot
    {
        public double AvgValue { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public int Hour { get; set; }
    }
}