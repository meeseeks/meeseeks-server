﻿namespace Meeseeks.Server.Core
{
    public enum BusType
    {
        Knx = 2,
        OneWire = 0,
        Manual = 1
    }
}