﻿using System;
using System.ComponentModel.DataAnnotations;
using Meeseeks.Server.Knx;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Meeseeks.Server.Core
{
    [BsonIgnoreExtraElements]
    public class Sensor
    {
        [Key]
        [BsonId]
        public Guid SensorId { get; set; }
        public string Description { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BusType Bus { get; set; }
        public string Address { get; set; }
        public SensorType Measure { get; set; }
        public bool IsRegistered { get; set; }
        [BsonElement("isknx")]
        public bool SendToKnx { get; set; }
        [BsonElement("knxga")]
        public string MappedGroupAddress { get; set; }
        [BsonElement("knxdpt")]
        public KnxDataPoint MappedDpt { get; set; }
        [BsonElement("room")]
        public string Room { get; set; }
    }
}
