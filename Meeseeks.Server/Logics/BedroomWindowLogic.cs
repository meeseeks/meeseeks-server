using System;
using System.Collections.Generic;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Intrgrations;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server.Logics
{
    public class BedroomWindowLogic : ILogic
    {
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;

        //Nur kipp
        private readonly List<string> _windowSensorList = new List<string> { "3/1/2", "3/1/4", "3/1/6" };

        private readonly ISensorManager _sensor;
        const string WindowsleepLastcheck = "windowsleep-lastcheck";


        public BedroomWindowLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
            _sensor = services.GetRequiredService<ISensorManager>();

        }

        public bool RunListener(EventArgs args)
        {
            //Timer every 5 min
            var timeargs = args as TimeEventArgs;
            if (timeargs == null) return false;

            DateTime lastCheck = _sm.ReadState<DateTime?>(WindowsleepLastcheck) ?? DateTime.MinValue;

            if (lastCheck == DateTime.MinValue)
            {
                _sm.WriteState(WindowsleepLastcheck, DateTime.Now);
                return false;
            }

            //run every 5 min only
            if (DateTime.Now.Subtract(lastCheck).TotalMinutes < 5)
            {
                return false;
            }

            //Record last check time
            _sm.WriteState(WindowsleepLastcheck, DateTime.Now);


            //read all window sensors
            bool windowCurrentlyOpen = false;
            foreach (string ga in _windowSensorList)
            {
                if (_ks.ReadSwitch(ga))
                {
                    windowCurrentlyOpen = true;
                    break;
                }
            }

            var tempZimmer = _sensor.GetCurrentValueAsync(BusType.Knx, "3/5/3").Result.Value;
            var tempAussen = _sensor.GetCurrentValueAsync(BusType.OneWire, "28.CCAB71020000").Result.Value;

            if (windowCurrentlyOpen)
            {
                _ks.SwitchOn("3/5/4");
            }
            else
            {
                _ks.SwitchOff("3/5/4");
            }

            bool windowslastTimeOpen = _sm.ReadState<bool?>("windowsleep-prev") ?? false;

            if (windowslastTimeOpen && windowCurrentlyOpen)
            {
                //Fenster waren schon auf. Zeit des letzten �ffnens holen
                DateTime lastOpenTime = _sm.ReadState<DateTime>("windowsleep-prevtime");

                if (DateTime.Now.Subtract(lastOpenTime).TotalMinutes > 30)
                {
                    if (tempAussen > 15)
                        return true;

                    if (tempZimmer > 18)
                        return true;

                    string slacktext = $"Im Schlafzimmer sind noch die Fenster offen ({tempZimmer}�C)";
                    SlackIntegration.SendText(slacktext);
                }
            }
            else if (windowslastTimeOpen)
            {
                //Fenster geschlossen
                _sm.WriteState("windowsleep-prev", false);
            }
            else if (windowCurrentlyOpen)
            {
                //Fenster wurden seit dem letzen check ge�ffnet
                _sm.WriteState("windowsleep-prev", true);
                _sm.WriteState("windowsleep-prevtime", DateTime.Now);
            }
            return true;
        }

        public string Name => nameof(BedroomWindowLogic);
    }
}