using System;
using System.Threading;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server.Logics
{
    public class JasminNightModeLogic : ILogic
    {
        private readonly IKnxSender _ks;

        public JasminNightModeLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
        }
        public bool RunListener(EventArgs args)
        {

            var knxargs = args as KnxBusEventArgs;
            if (knxargs == null) return false;

            if (knxargs.GroupAddress.Address != "12/3/0") return false;
            if (knxargs.Value != "1") return false;

            //enable nightmode
            _ks.SwitchOff("12/0/3"); //T�r
            _ks.SwitchOff("12/0/7"); //Nitte
            _ks.DimLights("12/0/13", 50); //Dim on bedlights

            //after 20 min dimm down to 20
            Thread.Sleep(new TimeSpan(0, 20, 0));
            _ks.DimLights("12/0/13", 25); //Dim on bedlights

            //after 5 min shutdown light completly
            Thread.Sleep(new TimeSpan(0, 5, 0));

            //preset lights for normal lighting
            _ks.DimLights("12/0/9", 60); //Dim on bedlights
            _ks.DimLights("12/0/5", 60); //Dim on bedlights
            _ks.DimLights("12/0/13", 30); //Dim on bedlights

            Thread.Sleep(5000); //Wait 5 sec to have presets applied then shut off lights
            _ks.SwitchOff("12/0/3"); //T�r
            _ks.SwitchOff("12/0/7"); //Nitte
            _ks.SwitchOff("12/0/11"); //Nitte

            return true;
        }

        public string Name => nameof(JasminNightModeLogic);
    }
}