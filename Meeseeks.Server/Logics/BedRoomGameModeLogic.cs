﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Logics
{

    public class RgbColor
    {
        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }
        public byte White { get; set; }
    }

    public class BedRoomGameModeLogic : ILogic
    {

        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;

        private readonly List<RgbColor> _stripColors;
        private readonly ILogger<BedRoomGameModeLogic> _logger;

        public BedRoomGameModeLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<BedRoomGameModeLogic>();

            _stripColors = new List<RgbColor>
            {
                new RgbColor {Red = 0,Green = 68,Blue = 48},
                new RgbColor {Red = 80,Green = 60,Blue = 0},
                new RgbColor {Red = 80,Green = 0,Blue = 70},
                new RgbColor {Red = 0,Green = 70,Blue = 0},
            };
        }

        public bool RunListener(EventArgs args)
        {
            //1. prüfe ob strom am game TV verbraucht wird
            var targs = args as TimeEventArgs;
            if (targs == null)
            {
                return false;
            }
            
            _logger.LogDebug("Got triggered");

            Ping pingSender = new Ping();
            var pingReply = pingSender.SendPingAsync(IPAddress.Parse("172.16.10.141")).Result;

            bool? inGameMode = _sm.ReadState<bool?>("gamemode");
            if (!inGameMode.HasValue)
            {
                inGameMode = false;
            }

            byte? lastColor = _sm.ReadState<byte?>("gamemodecolor");
            if (!lastColor.HasValue)
            {
                lastColor = 0;
            }

            bool hostOnline = pingReply.Status == IPStatus.Success;

            _logger.LogDebug("Gamehost is " + (hostOnline ? "online":"offline"));


            if (hostOnline && !inGameMode.Value)
            {
                
                //Host came online and we need to switch stuff on
                _ks.SwitchOn("3/0/0");

                //A nice blue
                SetStripColor(_stripColors[0]);

                _sm.WriteState("gamemode",true);
                return true;
            }

            if (!hostOnline && inGameMode.Value)
            {
                //Host offline disable lights
                _ks.SwitchOff("3/0/0");
                StripOff();
                _sm.WriteState("gamemode", false);
                return true;
            }

            if (hostOnline)
            {
                //Just cylce colors
                lastColor++;
                if (lastColor != null && lastColor.Value >= _stripColors.Count)
                {
                    lastColor = 0;
                }
                SetStripColor(_stripColors[lastColor.Value]);
                _sm.WriteState("gamemodecolor",lastColor.Value);

            }


            return false;
        }

        private void SetStripColor(RgbColor stripColor)
        {
            _ks.DimLights("3/2/0", stripColor.Red);
            _ks.DimLights("3/2/1", stripColor.Green);
            _ks.DimLights("3/2/2", stripColor.Blue);
        }

        private void StripOff()
        {
            _ks.DimLights("3/2/0", 0);
            _ks.DimLights("3/2/1", 0);
            _ks.DimLights("3/2/2", 0);
        }
        

        public string Name => nameof(BedRoomGameModeLogic);
    }
}