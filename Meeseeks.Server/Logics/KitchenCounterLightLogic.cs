using System;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server.Logics
{
    public class KitchenCounterLightLogic : ILogic
    {
        private readonly string _ledGa = "11/5/15";
        private readonly string _lockPmGa = "11/1/5";
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;

        public KitchenCounterLightLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
        }

        public bool RunListener(EventArgs args)
        {
            var timeargs = args as TimeEventArgs;
            if (timeargs == null) return false;


            bool? isNightMode = _sm.ReadState<bool?>("nightMode");

            if (isNightMode.HasValue && isNightMode.Value)
            {
                //Make sure strip is off during nightmode
                _ks.DimLights(_ledGa, 0);

                if (_ks.ReadSwitch(_lockPmGa)) //unlock PM when its locked so that during nightmode we see something
                    _ks.SwitchOff(_lockPmGa);

                return true;
            }

            DateTime currentDate = timeargs.CurrentDateTime;

            var time = currentDate.TimeOfDay;
            var wakeupTime = new TimeSpan(5, 30, 0);
            var moringTime = new TimeSpan(6, 15, 0);
            var fullDaylightTime = new TimeSpan(8, 0, 0);
            var afternoonStart = new TimeSpan(16, 0, 0);
            var dinerPrepareTime = new TimeSpan(18, 0, 0);
            var eveningTime = new TimeSpan(20, 0, 0);



            if (time > wakeupTime && time < moringTime) //between 5:30 and 6:15
            {
                _ks.DimLights(_ledGa, 60);
                _ks.SwitchOff(_lockPmGa); //unlock PM
            }
            else if (time > moringTime && time < fullDaylightTime) //between 6:15 and 8:00
            {
                _ks.DimLights(_ledGa, 100);
            }
            else if (time > fullDaylightTime && time < afternoonStart) //between 6:15 and 8:00
            {
                _ks.DimLights(_ledGa, 0);
            }
            else if (time > afternoonStart && time < dinerPrepareTime) //between 6:15 and 8:00
            {
                _ks.DimLights(_ledGa, 60);
            }
            else if (time > dinerPrepareTime && time < eveningTime) //between 6:15 and 8:00
            {
                _ks.DimLights(_ledGa, 100);
            }
            else if (time > eveningTime) //between 6:15 and 8:00
            {
                _ks.SwitchOn(_lockPmGa); //Lock PM so only strip is on
                _ks.DimLights(_ledGa, 60);
            }

            return true;
        }


        public string Name => nameof(KitchenCounterLightLogic);

    }
}