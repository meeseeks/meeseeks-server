using System;
using System.Globalization;
using System.Threading;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Meeseeks.Server.Speech;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server.Logics
{
    public class WakeUpMode : ILogic
    {
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;
        private readonly ISensorManager _sensormgr;
        private string _rabbitHost;

        public WakeUpMode(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
            _sensormgr = services.GetRequiredService<ISensorManager>();
            _rabbitHost = services.GetRequiredService<IConfigurationRoot>()["rabbithost"];

        }

        public bool RunListener(EventArgs args)
        {
            bool wakeHouseUp = false;

            var targs = args as TimeEventArgs;
            if (targs != null)
            {
                //Got a timer. Check if we need to wake the house
                DateTime cd = targs.CurrentDateTime;

                if ((cd.DayOfWeek == DayOfWeek.Saturday || cd.DayOfWeek == DayOfWeek.Sunday) &&
                    cd.Hour == 9 && cd.Minute == 0)
                {
                    //Wake up for Weekends at 8:30
                    wakeHouseUp = true;
                }
                else if ((cd.DayOfWeek != DayOfWeek.Saturday && cd.DayOfWeek != DayOfWeek.Sunday) &&
                         cd.Hour == 6 && cd.Minute == 0)
                {
                    //Wake up for weekdays at 6:00
                    wakeHouseUp = true;
                }
            }

            var wargs = args as WebEventArgs;
            if (wargs?.Name == "wakeup")
            {
                wakeHouseUp = true;
            }

            if (!wakeHouseUp)
            {
                return false;
            }

            WakeHouseAndSoundAlarmClock(DateTime.Now);
            return true;

        }

        public void WakeHouseAndSoundAlarmClock(DateTime cd)
        {
            //Enable nessesary house systems

            //switch on Wifi
            _ks.SwitchOn("3/3/6");

            CultureInfo ci = new CultureInfo("de-DE");

            //Get outside temperature
            double lastVal = _sensormgr.GetCurrentValueAsync(BusType.OneWire, "28.CCAB71020000").Result.Value;

            string speakText = $"Guten Morgen! Heute ist {cd.ToString("dddd", ci)} der {cd.Day}. {cd.ToString("MMMM", ci)}. " +
                               $"Draussen sind es im momment {lastVal:F0} grad.";

            SpeechSynthesis speech = new SpeechSynthesis(_rabbitHost);
            speech.SpeakToHouse(speakText, PlaybackLocations.Bedroom);

            //Wait a moment before playing radio
            Thread.Sleep(10000);
            speech.PlayRadio();

            bool awake = false;
            int count = 1;
            DateTime nextSpeak = DateTime.Now.AddMinutes(5);
            speech.ControlRadio("set_property volume 10");

            int lastDimValue = -1;

            while (!awake)
            {
                bool markusAwake = !_ks.ReadSwitch("3/6/2");
                bool silvanaAwake = !_ks.ReadSwitch("3/6/3");

                if (markusAwake || silvanaAwake)
                {
                    awake = true;
                }
                else
                {
                    var d = DateTime.Now;
                    if (d > nextSpeak)
                    {
                        string speaktext = $"Aufstehen. Es ist jetzt {d.Hour} Uhr {d.Minute}";
                        speech.SpeakToHouse(speaktext, PlaybackLocations.Bedroom);
                        speech.ControlRadio($"set_property volume {count * 10 + 20}");
                        nextSpeak = DateTime.Now.AddMinutes(5);
                    }

                    int dimValue = (count / 10) + 5;
                    if (lastDimValue != dimValue)
                    {
                        _ks.DimLights("3/2/3", dimValue);
                        lastDimValue = dimValue;
                    }
                    count++;

                    Thread.Sleep(1000);

                }
            }

            //We're awake
            speech.ControlRadio("quit");
            _ks.DimLights("3/2/3", 0); //switch off strip
            _ks.SwitchOff("3/4/1"); //unlock pm
            _sm.WriteState("nightMode", false);


            //Switch of switches
            _ks.SwitchOff("3/6/0");
            _ks.SwitchOff("3/6/1");

            //Switch of LED's
            _ks.SwitchOff("3/6/2");
            _ks.SwitchOff("3/6/3");

        }

        public string Name => nameof(WakeUpMode);
    }

}