﻿using System;
using System.Net.NetworkInformation;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Intrgrations;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Renci.SshNet;

namespace Meeseeks.Server.Logics
{
    public class LaforgeWakeUpLogic : ILogic
    {
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;
        const string StateKey = "laforge_alive";
        

        public LaforgeWakeUpLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();

        }

        public bool RunListener(EventArgs args)
        {
            var knxArgs = args as KnxBusEventArgs;
            var timeArgs = args as TimeEventArgs;
            if (knxArgs != null && knxArgs.GroupAddress.Address == "10/1/5")
            {
                HandleButton();
                return true;
            }

            if (timeArgs != null)
            {
                //ping laforge and set state accordingly
                Ping p = new Ping();
                PingReply pingResult = p.SendPingAsync("172.16.10.5").Result;

                bool laforgeRunning = pingResult.Status == IPStatus.Success;
                _sm.WriteState(StateKey, laforgeRunning);

                if (laforgeRunning)
                {
                    _ks.SwitchOff("10/1/7"); //switch off red led - green will show
                }
                else
                {
                    _ks.SwitchOn("10/1/7"); //switch on red led
                }

                return true;
            }


            return false;
        }

        private void HandleButton()
        {
            bool laforgeRunning = _sm.ReadState<bool>(StateKey);

            if (laforgeRunning)
            {
                SlackIntegration.SendText("LaForge shutdown");
                try
                {
                    //Shutdown Laforge
                    using (var client = new SshClient("172.16.10.5", "root", "9gJodFii"))
                    {
                        client.Connect();
                        client.RunCommand("poweroff");
                        client.Disconnect();
                    }
                }
                catch (Exception)
                {
                    // might be off already
                }
            }
            else
            {
                SlackIntegration.SendText("LaForge booting.");
                using (var client = new SshClient("172.16.20.177", "markus", "silvananet70319"))
                {
                    client.Connect();
                    client.RunCommand("sudo /home/markus/wake-laforge.sh");
                    client.Disconnect();
                }
            }
        }

        public string Name => nameof(LaforgeWakeUpLogic);
    }
}