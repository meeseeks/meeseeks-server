﻿using System;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Logics
{
    public class ElectricCounterLogic : ILogic
    {
        private readonly ISensorManager _sensorManager;
        private readonly IKnxSender _ks;
        private readonly ILogger<ElectricCounterLogic> _logger;
        private readonly IStateMachine _sm;

        public ElectricCounterLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sensorManager = services.GetRequiredService<ISensorManager>();
            var logfac = services.GetRequiredService<ILoggerFactory>();
            _logger = logfac.CreateLogger<ElectricCounterLogic>();
            _sm = services.GetRequiredService<IStateMachine>();

        }

        public bool RunListener(EventArgs args)
        {
            var timeArgs = args as TimeEventArgs;
            if (timeArgs == null) return false;

            double pricePerKwh = 0.288;

            string groupaddress = "0/0/3";
            string strCurrentCounter = _ks.ReadGroup(groupaddress, KnxDataPoint.Dpt13);

            double currentCounter = double.Parse(strCurrentCounter) / 1000; //to kWh
            _sensorManager.RecordValueAsync(BusType.Knx, groupaddress, currentCounter);

            double? objLastCounter = _sm.ReadState<double?>(BusType.Manual, "StromCounterLast");
            _sm.WriteState(BusType.Manual, "StromCounterLast", currentCounter);

            if (objLastCounter == null)
            {
                _logger.LogWarning("Recording electric energy no previous state");
                return false;
            }


            double lastCounter = objLastCounter.Value;

            double usedPower = currentCounter - lastCounter;
            var costOfValue = usedPower * pricePerKwh;
            _logger.LogInformation($"Recording electric energy use: {costOfValue} | {usedPower}");

            _sensorManager.RecordValueAsync(BusType.Manual, "ElecEnergy", usedPower);
            _sensorManager.RecordValueAsync(BusType.Manual, "Price/ElecEnergy", costOfValue);

            return true;
        }

        public string Name => nameof(ElectricCounterLogic);
    }
}