﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Meeseeks.Server.Controllers;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Meeseeks.Server.Speech;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Renci.SshNet;

namespace Meeseeks.Server.Logics
{
    public class NightModeLogic : ILogic
    {
        public NightModeLogic(IServiceProvider services)
        {
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<NightModeLogic>();
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
            _rabbitHost = services.GetRequiredService<IConfigurationRoot>()["rabbithost"];

        }

        private class NightGa : KnxSwitchInfo
        {
            public string BedLight { get; set; }
            public string Name { get; set; }

        }

        private readonly NightGa _gaMarkus = new NightGa { Name = "markus", BedLight = "3/0/11", Led = "3/6/2", Switch = "3/6/0" };
        private readonly NightGa _gaSilvana = new NightGa { Name = "silvana", BedLight = "3/0/10", Led = "3/6/3", Switch = "3/6/1" };
        private readonly string _wifiSwitch = "3/3/6";
        private readonly ILogger<NightModeLogic> _logger;
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;
        private string _rabbitHost;

        public bool RunListener(EventArgs args)
        {
            var eventArgs = args as KnxBusEventArgs;

            if (eventArgs == null) return false;

            if (eventArgs.GroupAddress.Address == _gaMarkus.Switch ||
                eventArgs.GroupAddress.Address == _gaSilvana.Switch)
            {
                return EnableNightMode(eventArgs);
            }

            return false;
        }

        private void ExecutePersonSpecificActions(NightGa ga,string gaValue)
        {
            bool? isOnAlready = _sm.ReadState<bool?>("night_" + ga.Name);
            bool switchOn = !isOnAlready.HasValue || !isOnAlready.Value;

            if (gaValue == "0") //make sure nightmode stays off
                switchOn = false;

            if (switchOn)
            {
                _ks.SwitchOn(ga.Led); //Enable Led
                _ks.DimLights(ga.BedLight, 30); //Dim on bedlights

                //Create timer to shutoff bed spot after 60sec
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(60000);
                    _ks.DimLights(ga.BedLight, 0);
                });
                _ks.SwitchOn("3/4/1"); //lock PM schlafzimmer

            }
            else
                _ks.SwitchOff(ga.Led); //otherwise shut off Led

            //Write state
            _sm.WriteState("night_" + ga.Name, switchOn);
        }

        private bool EnableNightMode(KnxBusEventArgs args)
        {

            //check which switch was pressed and blink Led
            string pressedGa = args.GroupAddress.Address;

            //1. write new state and execute person specific stuff
            if (pressedGa == _gaMarkus.Switch)
            {
                ExecutePersonSpecificActions(_gaMarkus,args.Value);
                _logger.LogInformation("Enable nightmode from markus");
            }
            else if (pressedGa == _gaSilvana.Switch)
            {
                ExecutePersonSpecificActions(_gaSilvana, args.Value);
            }

            //2. prüfe ob andere ga bereits aktiviert

            bool? silvanaActive = _sm.ReadState<bool?>("night_silvana");
            bool? markusActive = _sm.ReadState<bool?>("night_markus");

            if (silvanaActive.HasValue && silvanaActive.Value && markusActive.HasValue && markusActive.Value)
            {
                _logger.LogInformation("Enableing night mode...");

                SpeechSynthesis speech = new SpeechSynthesis(_rabbitHost);
                speech.SpeakToHouse("Nachtmodus aktiviert! Gute Nacht!", PlaybackLocations.Bedroom);

                _ks.SwitchOff("6/1/4"); //Licht Büro - PM gruppe
                _ks.SwitchOff("6/0/0"); //Licht Büro - alles
                _ks.SwitchOff(_wifiSwitch); //Wifi
                _ks.SwitchOff("3/0/6"); //Spots mitte schlafzimmer
                _ks.SwitchOff("7/0/12"); //Spots ankleide
                _ks.SwitchOff("0/1/0"); //Licht Technik
                _ks.SwitchOff("5/1/3"); //Licht Flur EG
                _ks.SwitchOff("14/0/4"); //Licht Flur OG

                NetworkApiController nac = new NetworkApiController();
                nac.ShutdownViaSsh("172.16.10.5","root","9gJodFii");

                _ks.SwitchOn("10/1/7");

                _sm.WriteState("nightMode", true);
                return true;
            }

            if (silvanaActive.HasValue && !silvanaActive.Value && markusActive.HasValue && !markusActive.Value)
            {
                _logger.LogInformation("Disabled night mode manually.");
                _ks.SwitchOff("3/4/1"); //unlock pm
                _sm.WriteState("nightMode", false);
                return true;
            }

            return false;
        }

        public string Name => nameof(NightModeLogic);

    }
}
