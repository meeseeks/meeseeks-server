using System;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Logics
{
    public class GasAndWaterCounterLogic : ILogic
    {
        private readonly IKnxSender _ks;
        private readonly ISensorManager _smgr;
        private readonly ILogger<GasAndWaterCounterLogic> _logger;

        public GasAndWaterCounterLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _smgr = services.GetRequiredService<ISensorManager>();
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<GasAndWaterCounterLogic>();
        }
        public bool RunListener(EventArgs args)
        {
            if (!(args is TimeEventArgs)) return false;

            RecordCounter("0/0/1", "0/0/6", "Wasser", 0.00199);
            RecordCounter("0/0/13", "0/0/14", "Gas", 0.5963);
            return true;
        }

        private void RecordCounter(string counterGa, string resetGa, string name, double cost)
        {
            string counter = _ks.ReadGroup(counterGa, KnxDataPoint.Dpt13);
            try
            {
                double value = double.Parse(counter);

                Console.WriteLine($"GAS: {counter}" );


                if (name == "Gas") //Gas records as 0.01m� per impulse
                    value = value/100.0;

                var costOfValue = value*cost;

                _smgr.RecordValueAsync(BusType.Knx, counterGa, value);
                _smgr.RecordValueAsync(BusType.Manual, "Price-" + name, costOfValue);

                _logger.LogInformation($"Recorded Counter={value} for {name}. Did us cost {costOfValue} Eur");

                //reset counter
                _ks.WriteGroup(resetGa, "1", KnxDataPoint.Dpt1);
            }
            catch (Exception e)
            {
                _logger.LogError($"Unable to parse/record value={counter} for {name}. \n {e}");
                throw;
            }
        }

        public string Name => nameof(GasAndWaterCounterLogic);
    }
}