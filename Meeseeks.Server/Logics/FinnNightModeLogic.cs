using System;
using System.Threading;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server.Logics
{
    public class FinnNightModeLogic : ILogic
    {
        private readonly IKnxSender _ks;

        public FinnNightModeLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();

        }
        public bool RunListener(EventArgs args)
        {
            var eventArgs = args as KnxBusEventArgs;
            if (eventArgs == null) return false;
            if (eventArgs.GroupAddress.Address != "13/3/0") return false;

            if (eventArgs.Value != "1") return false;

            //enable nightmode
            _ks.DimLights("13/0/5", 30); //Dim on bedlights

            //after 20 min dimm down to 20
            Thread.Sleep(new TimeSpan(0, 15, 0));
            _ks.DimLights("13/0/5", 20); //Dim on bedlights

            //after 5 min shutdown light completly
            Thread.Sleep(new TimeSpan(0, 5, 0));

            //preset lights for normal lighting
            _ks.SwitchOff("13/0/3"); //T�r
           
            return true;
        }

        public string Name => nameof(FinnNightModeLogic);
    }
}