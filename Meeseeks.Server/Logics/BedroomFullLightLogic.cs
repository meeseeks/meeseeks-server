using System;
using System.Threading;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Meeseeks.Server.Speech;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Meeseeks.Server.Logics
{
    public class BedroomFullLightLogic : ILogic
    {
        private readonly KnxSwitchInfo _gaSwitch = new KnxSwitchInfo { Led = "3/6/5", Switch = "3/6/4" };
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;
        private string _rabbitHost;

        public BedroomFullLightLogic(IServiceProvider services)
        {
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
            _rabbitHost = services.GetRequiredService<IConfigurationRoot>()["rabbithost"];

        }

        public bool RunListener(EventArgs args)
        {
            var knxargs = args as KnxBusEventArgs;
            if (knxargs == null) return false;
            if (knxargs.GroupAddress.Address != _gaSwitch.Switch) return false;

            bool? isOnAlready = _sm.ReadState<bool?>("bedroom_fulllight");

            bool isOn = isOnAlready ?? false;
            if (isOn)
            {
                _ks.SwitchOff("3/0/0");
                _ks.SwitchOff("3/0/2");
                _ks.SwitchOff("3/0/4");
                _ks.SwitchOff("3/0/6");

                _ks.SwitchOff("3/4/1"); //unlock PM schlafzimmer
                _ks.SwitchOff("3/6/5"); //LED Taster
                _sm.WriteState("bedroom_fulllight", false);

            }
            else
            {
                SpeechSynthesis speech = new SpeechSynthesis(_rabbitHost);
                speech.PlaySoundOnHouse("processing2.mp3", PlaybackLocations.Bedroom);
                _ks.SwitchOn("3/4/1"); //lock PM schlafzimmer
                _ks.SwitchOn("3/6/5"); //LED Taster

                _ks.SwitchOn("3/0/2"); //Spot silvana schlafzimmer
                Thread.Sleep(500);
                _ks.SwitchOff("3/6/5"); //LED Taster

                _ks.SwitchOn("3/0/4"); //Spot markus schlafzimmer
                Thread.Sleep(500);
                _ks.SwitchOn("3/6/5"); //LED Taster


                _ks.SwitchOn("3/0/0"); //Spot gaube schlafzimmer
                Thread.Sleep(500);
                _ks.SwitchOff("3/6/5"); //LED Taster


                _ks.SwitchOn("3/0/6"); //Spots mitte schlafzimmer
                _ks.SwitchOn("3/6/5"); //LED Taster
                _sm.WriteState("bedroom_fulllight", true);

            }

            return true;
        }
        public string Name => nameof(BedroomFullLightLogic);
    }
}