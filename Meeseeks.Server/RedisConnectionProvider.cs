﻿using System.Linq;
using System.Net;
using Meeseeks.Server.Interfaces;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace Meeseeks.Server
{
    public class RedisConnectionProvider : IRedisConnection
    {
        private readonly object _redislock = new object();
        private static ConnectionMultiplexer _redisCon;
        private readonly IConfigurationRoot _config;

        public RedisConnectionProvider(IConfigurationRoot config)
        {
            _config = config;
        }

        public ConnectionMultiplexer RedisConnection
        {
            get
            {
                if (_redisCon == null || !_redisCon.IsConnected)
                {
                    lock (_redislock)
                    {
                        if (_redisCon == null || !_redisCon.IsConnected)
                        {
                            var dnsTask = Dns.GetHostAddressesAsync(_config["Redis:Host"]);
                            var addresses = dnsTask.Result;
                            string redishost = string.Join(",", addresses.Select(x => x.MapToIPv4().ToString() + ":6379"));
                            _redisCon = ConnectionMultiplexer.Connect(redishost);
                        }
                    }
                }
                

                return _redisCon;
            }
        }

    }
}