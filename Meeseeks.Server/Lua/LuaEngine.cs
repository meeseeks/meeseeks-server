﻿using System;
using System.Threading;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Speech;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MoonSharp.Interpreter;

namespace Meeseeks.Server.Lua
{
    public class LuaEngine
    {
        private readonly Script _luaScript;
        private readonly IKnxSender _ks;
        private readonly IStateMachine _sm;
        private readonly string _rabbitHost;

        public LuaEngine(IServiceProvider services)
        {
            _luaScript = new Script();
            _ks = services.GetRequiredService<IKnxSender>();
            _sm = services.GetRequiredService<IStateMachine>();
            _rabbitHost = services.GetRequiredService<IConfigurationRoot>()["rabbithost"];

            RegisterCallBacks();
        }

        public void RunScript(string script)
        {
            _luaScript.DoString(script);
        }

        private void RegisterCallBacks()
        {
            _luaScript.Globals["KnxSend"] = (Func<string, string,bool>)KnxSendCallback;
            _luaScript.Globals["KnxRead"] = (Func<string, string>)KnxReadCallback;
            _luaScript.Globals["KnxSwitchOn"] = (Func<string, bool>)KnxSwitchOnCallback;
            _luaScript.Globals["KnxSwitchOff"] = (Func<string, bool>)KnxSwitchOffCallback;
            _luaScript.Globals["StateRead"] = (Func<string,string>)StateMachineReadCallback;
            _luaScript.Globals["StateWrite"] = (Func<string,string,bool>)StateMachineWriteCallback;
            _luaScript.Globals["PlaySound"] = (Func<string,string,bool>)PlaySoundCallback;
            _luaScript.Globals["Sleep"] = (Func<int,bool>)SleepCallback;
        }

        private string KnxReadCallback(string ga)
        {
            return _ks.ReadGroup(ga, KnxDataPoint.Dpt1);
        }

        private bool StateMachineWriteCallback(string name, string value)
        {
            _sm.WriteState(name,value);
            return true;
        }

        private bool KnxSwitchOnCallback(string ga)
        {
            _ks.SwitchOn(ga);
            return true;
        }

        private bool KnxSwitchOffCallback(string ga)
        {
            _ks.SwitchOff(ga);
            return true;
        }

        private bool SleepCallback(int seconds)
        {
            Thread.Sleep(seconds * 1000);
            return true;
        }

        private bool PlaySoundCallback(string location, string file)
        {
            PlaybackLocations loc;
            switch (location)
            {
                case "bedroom":
                    loc = PlaybackLocations.Bedroom;
                    break;
                case "hallway":
                    loc = PlaybackLocations.Hallway;
                    break;
                case "livingroom":
                    loc = PlaybackLocations.LivingRoom;
                    break;
                case "office":
                    loc = PlaybackLocations.Office;
                    break;
                default:
                    return false;
            }

            SpeechSynthesis speech = new SpeechSynthesis(_rabbitHost);
            speech.PlaySoundOnHouse(file, loc);
            return true;
        }

        private string StateMachineReadCallback(string name)
        {
            return _sm.ReadState<string>(name);
        }

        private bool KnxSendCallback(string ga,string value)
        {
            return false;
        }

       
    }
}
