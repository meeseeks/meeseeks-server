using System;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Meeseeks.Server.Watchers
{



    public sealed class RedisWatch
    {
        private readonly ILogger _logger;
        private readonly ISensorManager _sensMgr;
        private readonly IKnxManager _knxMgr;
        private readonly ConnectionMultiplexer _redis;

        public event EventHandler<KnxBusEventArgs> OnKnxEvent;


        public RedisWatch(IServiceProvider services)
        {
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<RedisWatch>();
            _sensMgr = services.GetRequiredService<ISensorManager>();
            _knxMgr = services.GetRequiredService<IKnxManager>();
            _redis = services.GetRequiredService<IRedisConnection>().RedisConnection;
        }

        public void Watch()
        {
            var sub = _redis.GetSubscriber();
            sub.Subscribe("knx_from_bus", (channel, msg) =>
            {
                //KNX Message
                //_logger.LogDebug("Got from redis:" + msg);
                var knxRedisEvent = JsonConvert.DeserializeObject<RedisKnxEvent>(msg);
                KnxGroupAddress ga = _knxMgr.FindGroupAddress(knxRedisEvent.Destination).Result;
                KnxDataPoint dpt = (KnxDataPoint)Enum.Parse(typeof(KnxDataPoint), knxRedisEvent.DataType, true);

                if (dpt != ga.DptFormat)
                {
                    _logger.LogWarning($"Updated GA type for {knxRedisEvent.Destination} => {knxRedisEvent.DataType}");

                    ga.DptFormat = dpt;
                    _knxMgr.UpdateGa(ga);
                }

                if (double.TryParse(knxRedisEvent.Value, out double sensorValue))
                {
                    _sensMgr.RecordValueAsync(BusType.Knx, knxRedisEvent.Destination, sensorValue);
                }

                //kick of any possible logics
                KnxBusEventArgs knxargs = new KnxBusEventArgs(ga, knxRedisEvent.Value);
                
                OnKnxEvent?.Invoke(this, knxargs);

            });
        }
    }
}