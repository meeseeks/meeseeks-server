using System;
using System.Threading;
using Meeseeks.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Meeseeks.Server.Watchers
{
    public sealed class TimeWatch
    {
        private readonly ILogger _logger;
        public event EventHandler<TimeEventArgs> TimerElapsed;
        private int _lastMinute;

        public TimeWatch(IServiceProvider services)
        {
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<TimeWatch>();
            _lastMinute = -1;
        }

        public void Watch()
        {
            while (true)
            {
                var dtn = DateTime.Now;

                if (_lastMinute != dtn.Minute)
                {
                    var triggerDt = new DateTime(dtn.Year, dtn.Month, dtn.Day, dtn.Hour, dtn.Minute, 0);

                    if (_lastMinute != -1)
                    {
                        _logger.LogInformation("Timer triggered");
                        OnTimerElapsed(new TimeEventArgs(triggerDt));
                    }
                    _lastMinute = dtn.Minute;
                }
                Thread.Sleep(10000);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void OnTimerElapsed(TimeEventArgs e)
        {
            TimerElapsed?.Invoke(this, e);
        }
    }
}