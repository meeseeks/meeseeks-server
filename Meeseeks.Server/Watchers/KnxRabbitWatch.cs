using System;
using System.Text;
using System.Threading;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Knx;
using Meeseeks.Server.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Meeseeks.Server.Watchers
{
    public class KnxRabbitWatch
    {
        private readonly ILogger _logger;
        private readonly ISensorManager _sensMgr;
        private readonly IKnxManager _knxMgr;
        private readonly IModel _channel;

        public event EventHandler<KnxBusEventArgs> OnKnxEvent;


        public KnxRabbitWatch(IServiceProvider services)
        {
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<RedisWatch>();
            _sensMgr = services.GetRequiredService<ISensorManager>();
            _knxMgr = services.GetRequiredService<IKnxManager>();
            var config = services.GetRequiredService<IConfigurationRoot>();
            var rabbitFactory = new ConnectionFactory { HostName = config["rabbithost"] };
            _channel = rabbitFactory.CreateConnection().CreateModel();
        }

        public void Watch()
        {


            string queueName = "knx_read_srv";
            _channel.QueueDeclare(queueName, true, false, false, null);
            _channel.QueueBind(queue: queueName,
                    exchange: "knx_read",
                    routingKey: "");


            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body);
                //Console.WriteLine("RabbitWatch Recieve: {0}", message);

                JObject knxMsg = JsonConvert.DeserializeObject<JObject>(message);

                string knxGa = knxMsg["dest"].Value<string>();
                string knxType = knxMsg["type"].Value<string>();

                if (knxType == "UNKN")
                {
                    _logger.LogError("Unable to process KNX event: " + message);
                    return;
                }
                string knxValue = knxMsg["value"].Value<string>();
                _logger.LogInformation("Processed KNX event: " + message);


                KnxGroupAddress ga = _knxMgr.FindGroupAddress(knxGa).Result;
                //KnxDataPoint dpt = (KnxDataPoint)Enum.Parse(typeof(KnxDataPoint), knxType, true);
                //if (dpt != ga.DptFormat)
                //{
                //    _logger.LogWarning($"Updated GA type for {knxGa} => {knxType}");

                //    ga.DptFormat = dpt;
                //    _knxMgr.UpdateGa(ga);
                //}
                if (double.TryParse(knxValue, out double sensorValue))
                {
                    _sensMgr.RecordValueAsync(BusType.Knx, knxGa, sensorValue);
                }
                    //kick of any possible logics
                    KnxBusEventArgs knxargs = new KnxBusEventArgs(ga, knxValue);

                OnKnxEvent?.Invoke(this, knxargs);


            };

            _channel.BasicConsume(queue: queueName,consumer: consumer);
        }
    }
}