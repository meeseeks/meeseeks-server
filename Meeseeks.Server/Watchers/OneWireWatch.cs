using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Meeseeks.Server.Core;
using Meeseeks.Server.Interfaces;
using Meeseeks.Server.Models;
using Microsoft.ApplicationInsights;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Meeseeks.Server.Watchers
{
    public sealed class OneWireWatch
    {
        private readonly ILogger _logger;
        private readonly ISensorManager _sensMgr;
        private readonly IKnxSender _knxsender;
        private readonly string _url;

        public event EventHandler<OneWireEventArgs> SensorRead;

        public OneWireWatch(IServiceProvider services)
        {
            _logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<OneWireWatch>();
            _sensMgr = services.GetRequiredService<ISensorManager>();
            _knxsender = services.GetRequiredService<IKnxSender>();
            _url = services.GetRequiredService<IConfigurationRoot>()["onewireUrl"];
        }

        private async Task<List<string>> FetchDevicesAsync()
        {
            _logger.LogInformation("Fetch Devices");

            using (HttpClient hc = new HttpClient())
            {
                var foo = await hc.GetStringAsync(_url + "onewire");

                var response = JsonConvert.DeserializeObject<Dictionary<string, object>>(foo);
                List<string> devices = ((JArray)response["devices"]).ToObject<List<string>>();
                _logger.LogInformation($"Got {devices.Count} devices");

                return devices;
            }
        }

        private async Task<double> ReadDeviceAsync(string device)
        {
            using (HttpClient hc = new HttpClient())
            {
                string response = await hc.GetStringAsync($"{_url}onewire/{device}");
                var devicedata = JsonConvert.DeserializeObject<Dictionary<string, object>>(response);

                if (devicedata == null) return 100;

                var currentValue = (double)devicedata["value"];
                //_logger.LogInformation($"Got {currentValue} from device {device}");
                return currentValue;
            }
        }


        public async void WatchAsync()
        {
            List<string> devices;
            try
            {
                devices = await FetchDevicesAsync();
            }
            catch (Exception)
            {
                return;
            }
            //check if devices are created in sensor db
            _sensMgr.EnsureSensorsCreated(BusType.OneWire, devices);

            while (true)
            {
                //check mountpoint for bus masters
                foreach (string device in devices)
                {
                    try
                    {
                        double currentValue = await ReadDeviceAsync(device);
                        if (currentValue > 80.0)
                            continue;
                        _sensMgr.RecordValueAsync(BusType.OneWire, device, currentValue);

                        Sensor s = await _sensMgr.GetSensorAsync(BusType.OneWire, device);
                        if (s.SendToKnx)
                        {
                            _logger.LogDebug($"Pass OneWire reading to KNX: {device} -> {s.MappedGroupAddress}");
                            _knxsender.WriteGroup(s.MappedGroupAddress,currentValue.ToString(CultureInfo.InvariantCulture),s.MappedDpt);
                        }
                        OnSensorRead(new OneWireEventArgs(s, currentValue));

                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"Error during fetch of OneWire Sensor {device}:{e}");
                        
                        TelemetryClient tc = new TelemetryClient();
                        tc.TrackException(new OneWireException($"Error during fetch of OneWire Sensor {device}",e));
                    }
                    Thread.Sleep(10000);
                }
            }
        }


        private void OnSensorRead(OneWireEventArgs e)
        {
            SensorRead?.Invoke(this, e);
        }
    }

    [Serializable]
    public class OneWireException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public OneWireException()
        {
        }

        public OneWireException(string message) : base(message)
        {
        }

        public OneWireException(string message, Exception inner) : base(message, inner)
        {
        }

        protected OneWireException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}