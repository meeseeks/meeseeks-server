﻿using System;
using System.Net;
using System.Net.Http;

namespace Meeseeks.Server.Intrgrations
{
    public class SlackIntegration
    {
        public static bool SendText(string text)
        {
            string slackurl = "https://hooks.slack.com/services/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";
            HttpContent content = new StringContent($"{{ \"text\": \"{text}\"}}");
            HttpClient hc = new HttpClient();
            var httpResult = hc.PostAsync(new Uri(slackurl), content).Result;
            return httpResult.StatusCode == HttpStatusCode.Accepted;
        }
    }
}